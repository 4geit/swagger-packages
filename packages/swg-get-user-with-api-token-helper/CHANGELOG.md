# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.28.0"></a>
# [1.28.0](https://gitlab.com/4geit/swagger-packages/compare/v1.27.0...v1.28.0) (2017-10-06)


### Features

* **paginate:** add paginate to the list endpoints ([209a863](https://gitlab.com/4geit/swagger-packages/commit/209a863))




<a name="1.15.1"></a>
## [1.15.1](https://gitlab.com/4geit/swagger-packages/compare/v1.15.0...v1.15.1) (2017-09-23)




**Note:** Version bump only for package @4geit/swg-get-user-with-api-token-helper

<a name="1.10.1"></a>
## [1.10.1](https://gitlab.com/4geit/swagger-packages/compare/v1.10.0...v1.10.1) (2017-09-18)




**Note:** Version bump only for package @4geit/swg-get-user-with-api-token-helper

<a name="1.6.3"></a>
## [1.6.3](https://gitlab.com/4geit/swagger-packages/compare/v1.6.2...v1.6.3) (2017-08-27)


### Bug Fixes

* **async/await:** support async/await ([61d7998](https://gitlab.com/4geit/swagger-packages/commit/61d7998))




<a name="1.5.0"></a>
# [1.5.0](https://gitlab.com/4geit/swagger-packages/compare/v1.4.0...v1.5.0) (2017-08-27)


### Features

* **packages:** import all other swagger packages ([0610f0a](https://gitlab.com/4geit/swagger-packages/commit/0610f0a))

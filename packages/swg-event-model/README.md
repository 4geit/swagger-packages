# @4geit/swg-event-model [![npm version](//badge.fury.io/js/@4geit%2Fswg-event-model.svg)](//badge.fury.io/js/@4geit%2Fswg-event-model)

---

event model for the event list feature

## Installation

1. A recommended way to install ***@4geit/swg-event-model*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-event-model) package manager using the following command:

```bash
npm i @4geit/swg-event-model --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-event-model
```

2. In your swagger file, you need to add a reference to the `SwgEventModel` definition under the `definitions` property (e.g. `/api/swagger/swagger.yaml`) as below:

```yaml
swagger: "2.0"
# ...
definitions:
  SwgEventModel:
    $ref: ../../node_modules/@4geit/swg-event-model/swagger.yaml#/definitions/SwgEventModel
# ...
```

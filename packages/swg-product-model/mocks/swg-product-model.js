'use strict'

const jsf = require('json-schema-faker')
const faker = require('faker')
faker.seed(24)
const Chance = require('chance')
const chance = new Chance(24)
const path = require('path')

const swgCheckAvailabilityHelper = require('@4geit/swg-check-availability-helper')
const swgUseDbHelper = require('@4geit/swg-use-db-helper')
const swgDereferenceSwaggerHelper = require('@4geit/swg-dereference-swagger-helper')
const swgGetUserWithApiTokenHelper = require('@4geit/swg-get-user-with-api-token-helper');
const swgDestructuringParametersHelper = require('@4geit/swg-destructuring-parameters-helper')
const swgPaginateHelper = require('@4geit/swg-paginate-helper')

module.exports = {
  productList: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          swgPaginateHelper(
            async ({ req, res, db, page, pageSize, user }) => {
              try {
                const name = req.swagger.params.name.value
                // define db collection to use
                const collection = db.collection('product')
                // get counter
                const counter = await collection.count({
                  user_id: user._id,
                  name: {
                    $regex: name || '',
                    $options: 'i',
                  },
                })
                res.append('X-TotalCount', counter)
                // get all items
                const docs = await collection.find({
                  user_id: user._id,
                  name: {
                    $regex: name || '',
                    $options: 'i',
                  },
                }).skip(pageSize * (page - 1)).limit(pageSize).toArray()
                // embeed
                const resolution = await Promise.all(docs.map(async ({ /*other_id,*/ user_id, ...fields }) => ({
                  ...fields,
                  user,
                  // other: await others.findOne({ _id: other_id }),
                })))
                // return result
                res.json(resolution)
              } catch (err) {
                console.log(err)
                res.status(500).json({
                  message: err
                })
              }
            }
          )
        )
      )
    )
  ),
  productAdd: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const { _id, id, user_id, /*otherId,*/ ...fields } = req.swagger.params.body.value
              // define db collection to use
              // const otherCollection = db.collection('others')
              const collection = db.collection('product')
              // find other
              // const otherItem = await otherCollection.findOne({
              //   id: otherId,
              // })
              // if (!otherItem) {
              //   // send an error object if otherItem is nil
              //   return res.status(403).json({
              //     message: 'Invalid other ID.'
              //   })
              // }
              // add the new user chatbox to the collection
              await collection.insert({
                id: chance.guid(),
                user_id: user._id,
                // other_id: other._id,
                ...fields,
              })
              // return message
              return res.status(201).json({
                message: 'added product item!'
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  productBulkAdd: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const body = req.swagger.params.body.value
              // define db collection to use
              // const otherCollection = db.collection('others')
              const collection = db.collection('product')
              // loop over bulk items
              await Promise.all(body.map(async ({ _id, id, user_id, /*otherId,*/ ...fields }) => {
                // find other
                // const otherItem = await otherCollection.findOne({
                //   id: otherId,
                // })
                // if (!otherItem) {
                //   // send an error object if otherItem is nil
                //   return res.status(403).json({
                //     message: 'Invalid other ID.'
                //   })
                // }
                // add the new product to the collection
                await collection.insert({
                  id: chance.guid(),
                  user_id: user._id,
                  // other_id: other._id,
                  ...fields,
                })
              }))
              // return message
              return res.status(201).json({
                message: 'added product items!'
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  productGet: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const id = req.swagger.params.id.value
              // define db collection to use
              // const otherCollection = db.collection('others')
              const collection = db.collection('product')
              // find item
              const item = await collection.findOne({
                id,
                user_id: user._id
              })
              if (!item) {
                // send an error object if item is nil
                return res.status(403).json({
                  message: 'Invalid product ID.'
                })
              }
              // embeed
              const { /*other_id,*/ user_id, ...fields } = item
              const resolution = {
                ...fields,
                user,
                // other: await otherCollection.findOne({ _id: other_id }),
              }
              // return item
              return res.status(200).json(resolution)
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  productDelete: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const id = req.swagger.params.id.value
              // define db collection to use
              const collection = db.collection('product')
              // delete item
              await collection.deleteOne({
                id,
                user_id: user._id,
              })
              // return message
              return res.status(200).json({
                message: 'product deleted!'
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  productUpdate: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const itemId = req.swagger.params.id.value
              const { _id, id, user_id, /*other_id,*/ ...fields } = req.swagger.params.body.value
              // define db collection to use
              const collection = db.collection('product')
              // udptae item
              if (!Object.keys(fields).length) {
                // send an error object if update failed
                return res.status(500).json({
                  message: 'No field to update.'
                })
              }
              const result = await collection.updateOne({
                id: itemId,
                user_id: user._id,
              }, { $set: { ...fields } })
              if (!result.modifiedCount) {
                // send an error object if update failed
                return res.status(500).json({
                  message: 'Update failed.'
                })
              }
              // return item
              return res.status(200).json({
                message: 'Update succeeded.'
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  productPopulate: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              // define db collection to use
              const collection = db.collection('product')
              // resolve swagger
              const { resolved: { definitions: { SwgProductModel } } } = await swgDereferenceSwaggerHelper(path.resolve(__dirname, '../swagger.yaml'))
              // add custom faker generator
              jsf.extend('faker', () => {
                faker.custom = {}
                return faker
              })
              // add custom chance generator
              jsf.extend('chance', () => {
                const custom = {
                  item: () => jsf(SwgProductModel),
                  setTrue: () => true,
                  setFalse: () => false,
                  tz: () => chance.timezone().abbr,
                }
                chance.mixin(custom)
                return chance
              })
              // create fake items and insert them to db
              await collection.insertMany(
                chance.n(chance.item, 10).map(({ user: dummyUser, ...fields }) => ({ user_id: user._id, ...fields }))
              )
              // return success message
              res.status(201).json({
                message: "Succeeded to populate fake product data to the db."
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  )
}

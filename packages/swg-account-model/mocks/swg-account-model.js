'use strict'

const jsf = require('json-schema-faker')
const faker = require('faker')
faker.seed(24)
const Chance = require('chance')
const chance = new Chance(24)
const path = require('path')

const swgCheckAvailabilityHelper = require('@4geit/swg-check-availability-helper')
const swgUseDbHelper = require('@4geit/swg-use-db-helper')
const swgDereferenceSwaggerHelper = require('@4geit/swg-dereference-swagger-helper')
const swgDestructuringParametersHelper = require('@4geit/swg-destructuring-parameters-helper')

module.exports = {
  populate: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        async ({ req, res, db }) => {
          try {
            // define db collection to use
            const users = db.collection('users')
            // resolve swagger
            const { resolved: { definitions: { SwgAccountModel } } } = await swgDereferenceSwaggerHelper(path.resolve(__dirname, '../swagger.yaml'))
            // add custom faker generator
            jsf.extend('faker', () => {
              faker.custom = {}
              return faker
            })
            // add custom chance generator
            jsf.extend('chance', () => {
              const custom = {
                user: () => jsf(SwgAccountModel),
                setTrue: () => true,
                setFalse: () => false,
              }
              chance.mixin(custom)
              return chance
            })
            // create fake users and insert them to db
            await users.insertMany(
              chance.n(chance.user, 10)
            )
            // return success message
            res.status(201).json({
              message: "Succeeded to populate fake user data to the db."
            })
          } catch (err) {
            console.log(err)
            res.status(500).json({
              message: err
            })
          }
        }
      )
    )
  ),
  users: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        async ({ req, res, db }) => {
          // define db collection to use
          const users = db.collection('users')
          // get all users
          const docs = await users.find().toArray()
          // return result
          res.json(docs)
        }
      )
    )
  ),
}

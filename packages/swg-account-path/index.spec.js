'use strict'

const Swagmock = require('swagmock')
const assert = require('assert')
const swgDereferenceSwaggerHelper = require('@4geit/swg-dereference-swagger-helper')

describe('swg-account-path', () => {
  // build swagmock object
  before(async () => {
    const { resolved } = await swgDereferenceSwaggerHelper()
    this.mockgen = Swagmock(resolved)
  })
  // account endpoint
  describe('account', () => {
    const path = '/account'
    const operation = 'get'

    it('has to contains the correct parameters', async () => {
      try {
        const { parameters } = await this.mockgen.parameters({
          path,
          operation,
        })
        assert.ok(parameters, 'generated parameters')
        assert.deepEqual(Object.keys(parameters), [])
      } catch (err) {
        assert.ifError(err)
      }
    })
    it('has to contains the correct response fields', async () => {
      try {
        const { responses } = await this.mockgen.responses({
          path,
          operation,
          response: 200,
        })
        assert.ok(responses, 'generated responses')
        assert.deepEqual(Object.keys(responses), [
          'id',
          'email',
          'password',
          'token',
          'tokens',
          'roles',
          'firstname',
          'lastname',
          'company',
          'address',
          'phone',
          'active',
          'diff1',
          'diff2',
          'diff3'
        ])
      } catch (err) {
        assert.ifError(err)
      }
    })
  })
})

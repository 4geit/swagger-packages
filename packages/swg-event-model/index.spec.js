'use strict'

const expect = require('chai').expect
const request = require('supertest')
const Swagmock = require('swagmock')
const MongoClient = require('mongodb').MongoClient
const swgDereferenceSwaggerHelper = require('@4geit/swg-dereference-swagger-helper')
const _app = require('../../app')

describe('swg-event-model', () => {
  // build swagmock object
  before(async () => {
    const { resolved } = await swgDereferenceSwaggerHelper()
    this.mockgen = Swagmock(resolved)
    this.app = await _app
  })
  beforeEach(async () => {
    this.app.locals.db = await MongoClient.connect('mongodb://db/test')
  })
  afterEach(async () => {
    await this.app.locals.db.dropDatabase()
  })
  // list endpoint
  describe('list', () => {
    const path = '/events'
    const operation = 'get'

    it('has to contains the correct parameters', async () => {
      const { parameters } = await this.mockgen.parameters({
        path,
        operation,
      })
      expect(parameters).to.not.be.undefined
      expect(Object.keys(parameters)).to.deep.equal([])
    })
    it('has to contains the correct response fields', async () => {
      const { responses } = await this.mockgen.responses({
        path,
        operation,
        response: 200,
      })
      expect(responses).to.not.be.undefined
      expect(responses).to.have.lengthOf.at.least(1)
      const [ response ] = responses
      expect(Object.keys(response)).to.deep.equal([
        'id',
        'activity',
        'timeslots',
      ])
    })
    it('returns status code 200', async () => {
      const response = await request(this.app).get('/v1/events')
      expect(response.statusCode).to.equal(200)
    })
    it('returns an empty array', async () => {
      const response = await request(this.app).get('/v1/events')
      expect(response.body).to.deep.equal([])
    })
  })
})

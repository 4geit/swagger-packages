'use strict'

module.exports = cb => {
  return (req, res) => {
    return cb({ req, res })
  }
}

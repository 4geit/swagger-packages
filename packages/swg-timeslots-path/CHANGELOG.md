# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.31.0"></a>
# [1.31.0](https://gitlab.com/4geit/swagger-packages/compare/v1.30.2...v1.31.0) (2017-10-22)


### Features

* **test environment:** setup the new test environment ([5f492cb](https://gitlab.com/4geit/swagger-packages/commit/5f492cb))




<a name="1.28.0"></a>
# [1.28.0](https://gitlab.com/4geit/swagger-packages/compare/v1.27.0...v1.28.0) (2017-10-06)




**Note:** Version bump only for package @4geit/swg-timeslots-path

<a name="1.15.1"></a>
## [1.15.1](https://gitlab.com/4geit/swagger-packages/compare/v1.15.0...v1.15.1) (2017-09-23)




**Note:** Version bump only for package @4geit/swg-timeslots-path

<a name="1.14.0"></a>
# [1.14.0](https://gitlab.com/4geit/swagger-packages/compare/v1.13.1...v1.14.0) (2017-09-21)


### Features

* **timeslots:** add page param ([addc7a5](https://gitlab.com/4geit/swagger-packages/commit/addc7a5))




<a name="1.13.1"></a>
## [1.13.1](https://gitlab.com/4geit/swagger-packages/compare/v1.13.0...v1.13.1) (2017-09-21)


### Bug Fixes

* **timeslots:** fix param name ([563e7d0](https://gitlab.com/4geit/swagger-packages/commit/563e7d0))




<a name="1.13.0"></a>
# [1.13.0](https://gitlab.com/4geit/swagger-packages/compare/v1.12.0...v1.13.0) (2017-09-20)


### Features

* **timeslots-path:** add security definition to use the token header ([7134024](https://gitlab.com/4geit/swagger-packages/commit/7134024))




<a name="1.12.0"></a>
# [1.12.0](https://gitlab.com/4geit/swagger-packages/compare/v1.11.1...v1.12.0) (2017-09-20)


### Features

* **scripts:** add endpoints to model type + fix issue on timeslots path ([3078c29](https://gitlab.com/4geit/swagger-packages/commit/3078c29))




<a name="1.11.1"></a>
## [1.11.1](https://gitlab.com/4geit/swagger-packages/compare/v1.11.0...v1.11.1) (2017-09-20)


### Bug Fixes

* **timeslots-path:** fix minor issue ([d9d4574](https://gitlab.com/4geit/swagger-packages/commit/d9d4574))




<a name="1.11.0"></a>
# [1.11.0](https://gitlab.com/4geit/swagger-packages/compare/v1.10.1...v1.11.0) (2017-09-20)


### Features

* **timeslots-path:** add a new endpoint definition ([55a0f63](https://gitlab.com/4geit/swagger-packages/commit/55a0f63))

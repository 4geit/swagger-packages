'use strict'

const util = require('util')

const swgDestructuringParametersHelper = require('@4geit/swg-destructuring-parameters-helper')

module.exports = {
  hello: swgDestructuringParametersHelper(
    ({ req, res }) => {
      var name = req.swagger.params.name.value || 'stranger'
      var hello = util.format('Hello, %s!', name)
      res.json(hello)
    }
  )
}

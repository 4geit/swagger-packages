'use strict'

const expect = require('chai').expect
const request = require('supertest')
const Swagmock = require('swagmock')
const MongoClient = require('mongodb').MongoClient
const swgDereferenceSwaggerHelper = require('@4geit/swg-dereference-swagger-helper')
const _app = require('../../app')

describe('swg-product-model', () => {
  // build swagmock object
  before(async () => {
    const { resolved } = await swgDereferenceSwaggerHelper()
    this.mockgen = Swagmock(resolved)
    this.app = await _app
  })
  beforeEach(async () => {
    this.app.locals.db = await MongoClient.connect('mongodb://db/test')
  })
  afterEach(async () => {
    await this.app.locals.db.dropDatabase()
  })
  // list endpoint
  describe('list', () => {
    const path = '/products'
    const operation = 'get'

    it('has to contains the correct parameters', async () => {
      const { parameters } = await this.mockgen.parameters({
        path,
        operation,
      })
      expect(parameters).to.not.be.undefined
      expect(Object.keys(parameters)).to.deep.equal([
        'query',
      ])
      const { query } = parameters
      expect(query).to.not.be.undefined
      expect(query.map(({ name }) => name)).to.deep.equal([
        'page',
        'pageSize'
      ])
    })
    it('has to contains the correct response fields', async () => {
      const { responses } = await this.mockgen.responses({
        path,
        operation,
        response: 200,
      })
      expect(responses).to.not.be.undefined
      expect(responses).to.have.lengthOf.at.least(1)
      const [ response ] = responses
      expect(Object.keys(response)).to.deep.equal([
        'id',
        'user',
        'bloomberCode',
        'type',
        'name',
        'group',
        'currency',
        'productIsIn',
        'underlyingIsIn',
        'shareIsIn',
        'contractSize',
        'contractMonths',
        'minPriceChange',
        'countryCode',
        'groupId',
        'cashMarketId',
        'sector',
        'stoxxEurope600',
        'euroStoxx50',
        'stoxxEurope50',
        'dax',
        'smi',
        'mdax',
        'sdax',
        'tecDax',
        'smim',
        'stoxx200Large',
        'stoxx200Mid',
        'stoxx200Small',
        'DJGlobalTitans50Sm',
        'aexIndex',
        'cac40',
        'sp500',
        'rdxXtUsdRdxExtended',
        'ftse100',
        'atx',
      ])
    })
    it('returns status code 403', async () => {
      const response = await request(this.app).get('/v1/products')
      expect(response.statusCode).to.equal(403)
    })
    it('returns an error message', async () => {
      const response = await request(this.app).get('/v1/products')
      expect(response.body).to.deep.equal({
        message: 'Authorization has been denied for this request. (token is missing)',
        code: 'server_error',
        statusCode: 403,
      })
    })
  })
  // add endpoint
  describe('add', () => {
    const path = '/products'
    const operation = 'post'

    it('has to contains the correct parameters', async () => {
      const { parameters } = await this.mockgen.parameters({
        path,
        operation,
      })
      expect(parameters).to.not.be.undefined
      expect(Object.keys(parameters)).to.deep.equal([
        'body',
      ])
      const { body } = parameters
      expect(body).to.not.be.undefined
      expect(body.map(({ name }) => name)).to.deep.equal([
        'body',
      ])
      const [ { value } ] = body
      expect(value).to.not.be.undefined
      expect(Object.keys(value)).to.deep.equal([
        'bloomberCode',
        'type',
        'name',
        'group',
        'currency',
        'productIsIn',
        'underlyingIsIn',
        'shareIsIn',
        'contractSize',
        'contractMonths',
        'minPriceChange',
        'countryCode',
        'groupId',
        'cashMarketId',
        'sector',
        'stoxxEurope600',
        'euroStoxx50',
        'stoxxEurope50',
        'dax',
        'smi',
        'mdax',
        'sdax',
        'tecDax',
        'smim',
        'stoxx200Large',
        'stoxx200Mid',
        'stoxx200Small',
        'DJGlobalTitans50Sm',
        'aexIndex',
        'cac40',
        'sp500',
        'rdxXtUsdRdxExtended',
        'ftse100',
        'atx',
      ])
    })
    it('has to contains the correct response fields', async () => {
      const { responses } = await this.mockgen.responses({
        path,
        operation,
        response: 201,
      })
      expect(responses).to.not.be.undefined
      expect(Object.keys(responses)).to.deep.equal([
        'message',
      ])
    })
    it('returns status code 403', async () => {
      const response = await request(this.app).post('/v1/products')
      expect(response.statusCode).to.equal(403)
    })
    it('returns an error message', async () => {
      const response = await request(this.app).post('/v1/products')
      expect(response.body).to.deep.equal({
        message: 'Authorization has been denied for this request. (token is missing)',
        code: 'server_error',
        statusCode: 403,
      })
    })
  })
  // get endpoint
  describe('get', () => {
    const path = '/products/{id}'
    const operation = 'get'

    it('has to contains the correct parameters', async () => {
      const { parameters } = await this.mockgen.parameters({
        path,
        operation,
      })
      expect(parameters).to.not.be.undefined
      expect(Object.keys(parameters)).to.deep.equal([
        'path',
      ])
      const { path: pathParam } = parameters
      expect(pathParam).to.not.be.undefined
      expect(pathParam.map(({ name }) => name)).to.deep.equal([
        'id',
      ])
    })
    it('has to contains the correct response fields', async () => {
      const { responses } = await this.mockgen.responses({
        path,
        operation,
        response: 200,
      })
      expect(responses).to.not.be.undefined
      expect(Object.keys(responses)).to.deep.equal([
        'id',
        'user',
        'bloomberCode',
        'type',
        'name',
        'group',
        'currency',
        'productIsIn',
        'underlyingIsIn',
        'shareIsIn',
        'contractSize',
        'contractMonths',
        'minPriceChange',
        'countryCode',
        'groupId',
        'cashMarketId',
        'sector',
        'stoxxEurope600',
        'euroStoxx50',
        'stoxxEurope50',
        'dax',
        'smi',
        'mdax',
        'sdax',
        'tecDax',
        'smim',
        'stoxx200Large',
        'stoxx200Mid',
        'stoxx200Small',
        'DJGlobalTitans50Sm',
        'aexIndex',
        'cac40',
        'sp500',
        'rdxXtUsdRdxExtended',
        'ftse100',
        'atx',
      ])
    })
    it('returns status code 403', async () => {
      const response = await request(this.app).get('/v1/products/123')
      expect(response.statusCode).to.equal(403)
    })
    it('returns an error message', async () => {
      const response = await request(this.app).get('/v1/products/123')
      expect(response.body).to.deep.equal({
        message: 'Authorization has been denied for this request. (token is missing)',
        code: 'server_error',
        statusCode: 403,
      })
    })
  })
  // delete endpoint
  describe('delete', () => {
    const path = '/products/{id}'
    const operation = 'delete'

    it('has to contains the correct parameters', async () => {
      const { parameters } = await this.mockgen.parameters({
        path,
        operation,
      })
      expect(parameters).to.not.be.undefined
      expect(Object.keys(parameters)).to.deep.equal([
        'path',
      ])
      const { path: pathParam } = parameters
      expect(pathParam).to.not.be.undefined
      expect(pathParam.map(({ name }) => name)).to.deep.equal([
        'id',
      ])
    })
    it('has to contains the correct response fields', async () => {
      const { responses } = await this.mockgen.responses({
        path,
        operation,
        response: 200,
      })
      expect(responses).to.not.be.undefined
      expect(Object.keys(responses)).to.deep.equal([
        'message',
      ])
    })
    it('returns status code 403', async () => {
      const response = await request(this.app).delete('/v1/products/123')
      expect(response.statusCode).to.equal(403)
    })
    it('returns an error message', async () => {
      const response = await request(this.app).delete('/v1/products/123')
      expect(response.body).to.deep.equal({
        message: 'Authorization has been denied for this request. (token is missing)',
        code: 'server_error',
        statusCode: 403,
      })
    })
  })
  // update endpoint
  describe('update', () => {
    const path = '/products/{id}'
    const operation = 'put'

    it('has to contains the correct parameters', async () => {
      const { parameters } = await this.mockgen.parameters({
        path,
        operation,
      })
      expect(parameters).to.not.be.undefined
      expect(Object.keys(parameters)).to.deep.equal([
        'path',
        'body'
      ])
      const { path: pathParam, body } = parameters
      expect(pathParam).to.not.be.undefined
      expect(body).to.not.be.undefined
      expect(pathParam.map(({ name }) => name)).to.deep.equal([
        'id',
      ])
      expect(body.map(({ name }) => name)).to.deep.equal([
        'body',
      ])
      const [ { value } ] = body
      expect(value).to.not.be.undefined
      expect(Object.keys(value)).to.deep.equal([
        'bloomberCode',
        'type',
        'name',
        'group',
        'currency',
        'productIsIn',
        'underlyingIsIn',
        'shareIsIn',
        'contractSize',
        'contractMonths',
        'minPriceChange',
        'countryCode',
        'groupId',
        'cashMarketId',
        'sector',
        'stoxxEurope600',
        'euroStoxx50',
        'stoxxEurope50',
        'dax',
        'smi',
        'mdax',
        'sdax',
        'tecDax',
        'smim',
        'stoxx200Large',
        'stoxx200Mid',
        'stoxx200Small',
        'DJGlobalTitans50Sm',
        'aexIndex',
        'cac40',
        'sp500',
        'rdxXtUsdRdxExtended',
        'ftse100',
        'atx',
      ])
    })
    it('has to contains the correct response fields', async () => {
      const { responses } = await this.mockgen.responses({
        path,
        operation,
        response: 200,
      })
      expect(responses).to.not.be.undefined
      expect(Object.keys(responses)).to.deep.equal([
        'message',
      ])
    })
    it('returns status code 403', async () => {
      const response = await request(this.app).put('/v1/products/123')
      expect(response.statusCode).to.equal(403)
    })
    it('returns an error message', async () => {
      const response = await request(this.app).put('/v1/products/123')
      expect(response.body).to.deep.equal({
        message: 'Authorization has been denied for this request. (token is missing)',
        code: 'server_error',
        statusCode: 403,
      })
    })
  })
})

# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.31.5"></a>
## [1.31.5](https://gitlab.com/4geit/swagger-packages/compare/v1.31.4...v1.31.5) (2017-11-24)


### Bug Fixes

* **chatbox-model:** add userChatbox field as a result of chatboxStatusList ([161a11b](https://gitlab.com/4geit/swagger-packages/commit/161a11b))




<a name="1.31.3"></a>
## [1.31.3](https://gitlab.com/4geit/swagger-packages/compare/v1.31.2...v1.31.3) (2017-11-14)




**Note:** Version bump only for package @4geit/swg-chatbox-model

<a name="1.31.1"></a>
## [1.31.1](https://gitlab.com/4geit/swagger-packages/compare/v1.31.0...v1.31.1) (2017-10-25)




**Note:** Version bump only for package @4geit/swg-chatbox-model

<a name="1.31.0"></a>
# [1.31.0](https://gitlab.com/4geit/swagger-packages/compare/v1.30.2...v1.31.0) (2017-10-22)


### Features

* **jest:** convert to jest test environment ([95b423f](https://gitlab.com/4geit/swagger-packages/commit/95b423f))




<a name="1.28.1"></a>
## [1.28.1](https://gitlab.com/4geit/swagger-packages/compare/v1.28.0...v1.28.1) (2017-10-06)


### Bug Fixes

* **chatbox-model:** fix status endpoint issue ([d8ca1df](https://gitlab.com/4geit/swagger-packages/commit/d8ca1df))




<a name="1.28.0"></a>
# [1.28.0](https://gitlab.com/4geit/swagger-packages/compare/v1.27.0...v1.28.0) (2017-10-06)




**Note:** Version bump only for package @4geit/swg-chatbox-model

<a name="1.20.0"></a>
# [1.20.0](https://gitlab.com/4geit/swagger-packages/compare/v1.19.5...v1.20.0) (2017-09-27)


### Features

* **chatbox-model:** add new status endpoint to know if a chatbox has been already added to a user l ([362d0d9](https://gitlab.com/4geit/swagger-packages/commit/362d0d9))

# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.32.0"></a>
# [1.32.0](https://gitlab.com/4geit/swagger-packages/compare/v1.31.7...v1.32.0) (2017-12-01)


### Features

* **contact,product:** add filtering parameter to list endpoint ([27058d7](https://gitlab.com/4geit/swagger-packages/commit/27058d7))




<a name="1.31.7"></a>
## [1.31.7](https://gitlab.com/4geit/swagger-packages/compare/v1.31.6...v1.31.7) (2017-12-01)


### Bug Fixes

* **contact-model, product-model:** wire pagination parameters ([7363430](https://gitlab.com/4geit/swagger-packages/commit/7363430))




<a name="1.31.1"></a>
## [1.31.1](https://gitlab.com/4geit/swagger-packages/compare/v1.31.0...v1.31.1) (2017-10-25)




**Note:** Version bump only for package @4geit/swg-product-model

<a name="1.31.0"></a>
# [1.31.0](https://gitlab.com/4geit/swagger-packages/compare/v1.30.2...v1.31.0) (2017-10-22)


### Features

* **jest:** convert to jest test environment ([95b423f](https://gitlab.com/4geit/swagger-packages/commit/95b423f))




<a name="1.30.0"></a>
# [1.30.0](https://gitlab.com/4geit/swagger-packages/compare/v1.29.1...v1.30.0) (2017-10-10)


### Features

* **product-model:** add bulk add endpoint ([0ea7ea7](https://gitlab.com/4geit/swagger-packages/commit/0ea7ea7))




<a name="1.28.0"></a>
# [1.28.0](https://gitlab.com/4geit/swagger-packages/compare/v1.27.0...v1.28.0) (2017-10-06)




**Note:** Version bump only for package @4geit/swg-product-model

<a name="1.25.3"></a>
## [1.25.3](https://gitlab.com/4geit/swagger-packages/compare/v1.25.2...v1.25.3) (2017-10-05)


### Bug Fixes

* **product-model:** disabled enum ([ea48a26](https://gitlab.com/4geit/swagger-packages/commit/ea48a26))
* **product-model:** replace boolean field by string ([df3b157](https://gitlab.com/4geit/swagger-packages/commit/df3b157))




<a name="1.25.2"></a>
## [1.25.2](https://gitlab.com/4geit/swagger-packages/compare/v1.25.1...v1.25.2) (2017-10-05)


### Bug Fixes

* **product-model:** replace fields type from number to string ([8a80d5e](https://gitlab.com/4geit/swagger-packages/commit/8a80d5e))




<a name="1.25.1"></a>
## [1.25.1](https://gitlab.com/4geit/swagger-packages/compare/v1.25.0...v1.25.1) (2017-10-05)


### Bug Fixes

* **product/contact model:** add endpoint with required fields ([f4910dd](https://gitlab.com/4geit/swagger-packages/commit/f4910dd))




<a name="1.19.4"></a>
## [1.19.4](https://gitlab.com/4geit/swagger-packages/compare/v1.19.3...v1.19.4) (2017-09-26)


### Bug Fixes

* **swagger:** missing security def ([82627d6](https://gitlab.com/4geit/swagger-packages/commit/82627d6))




<a name="1.19.0"></a>
# [1.19.0](https://gitlab.com/4geit/swagger-packages/compare/v1.18.0...v1.19.0) (2017-09-24)


### Features

* **product/contact:** CRUD operators/endpoints ([5b5d978](https://gitlab.com/4geit/swagger-packages/commit/5b5d978))

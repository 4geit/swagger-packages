# @4geit/swg-create-or-load-data-helper [![npm version](//badge.fury.io/js/@4geit%2Fswg-create-or-load-data-helper.svg)](//badge.fury.io/js/@4geit%2Fswg-create-or-load-data-helper)

---

helper function to create or load a data file with a data fake generator

## Installation

1. A recommended way to install ***@4geit/swg-create-or-load-data-helper*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-create-or-load-data-helper) package manager using the following command:

```bash
npm i @4geit/swg-create-or-load-data-helper --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-create-or-load-data-helper
```

2. All you have to do is to import the `@4geit/swg-create-or-load-data-helper` package in your controller or mock file as below:

```js
// ...
const swgCreateOrLoadDataHelper = require('@4geit/swg-create-or-load-data-helper');
// ...
```

And use it with one of your endpoint controller as below:

```js
// ...
const data = swgCreateOrLoadDataHelper(filename, library, generator, n)
// ...
```

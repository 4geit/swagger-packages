definitions:
  SwgChatboxModel:
    required:
      - id
      - name
      - description
    properties:
      id:
        type: string
        format: uuid
        x-chance: guid
      name:
        type: string
        x-faker: company.companyName
      description:
        type: string
        x-faker: company.catchPhrase
  ChatboxListPath:
    x-swagger-router-controller: swg-chatbox-model
    get:
      operationId: chatboxList
      description: |
        list all the chatbox entries
      tags:
        - Account
      security:
        - $ref: ../swg-account-security-model/swagger.yaml#/definitions/OperationSecurityItem
      parameters:
        - name: page
          in: query
          description: page number
          required: false
          type: integer
        - name: pageSize
          in: query
          description: page size
          required: false
          type: integer
      responses:
        200:
          description: Success
          schema:
            type: array
            items:
              $ref: "#/definitions/SwgChatboxModel"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"
    post:
      operationId: chatboxAdd
      description: |
        add a new chatbox
      tags:
        - Account
      security:
        - $ref: ../swg-account-security-model/swagger.yaml#/definitions/OperationSecurityItem
      parameters:
        - name: body
          in: body
          description: An object to set chatbox model fields
          required: true
          schema:
            $ref: '#/definitions/AddParameter'
      responses:
        201:
          description: Success
          schema:
            $ref: "#/definitions/SuccessResponse"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"
  ChatboxItemPath:
    x-swagger-router-controller: swg-chatbox-model
    get:
      operationId: chatboxGet
      description: |
        get a chatbox entry
      tags:
        - Account
      security:
        - $ref: ../swg-account-security-model/swagger.yaml#/definitions/OperationSecurityItem
      parameters:
        - name: id
          in: path
          description: A chatbox ID
          required: true
          type: string
      responses:
        200:
          description: Success
          schema:
            $ref: "#/definitions/SwgChatboxModel"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"
    delete:
      operationId: chatboxDelete
      description: |
        delete a chatbox entry
      tags:
        - Account
      security:
        - $ref: ../swg-account-security-model/swagger.yaml#/definitions/OperationSecurityItem
      parameters:
        - name: id
          in: path
          description: A chatbox ID
          required: true
          type: string
      responses:
        200:
          description: Success
          schema:
            $ref: "#/definitions/SuccessResponse"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"
    put:
      operationId: chatboxUpdate
      description: |
        update a chatbox entry
      tags:
        - Account
      security:
        - $ref: ../swg-account-security-model/swagger.yaml#/definitions/OperationSecurityItem
      parameters:
        - name: id
          in: path
          description: A chatbox ID
          required: true
          type: string
        - name: body
          in: body
          description: An object to pass new field values
          required: true
          schema:
            $ref: '#/definitions/UpdateParameter'
      responses:
        200:
          description: Success
          schema:
            $ref: "#/definitions/SuccessResponse"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"
  ChatboxStatusListPath:
    x-swagger-router-controller: swg-chatbox-model
    get:
      operationId: chatboxStatusList
      description: |
        list all the chatbox entries and their status
      tags:
        - Account
      security:
        - $ref: ../swg-account-security-model/swagger.yaml#/definitions/OperationSecurityItem
      # parameters:
      #   - name: name
      #     in: query
      #     description: Dummy parameter
      #     required: false
      #     type: string
      responses:
        200:
          description: Success
          schema:
            type: array
            items:
              type: object
              required:
                - chatbox
                - status
              properties:
                chatbox:
                  $ref: "#/definitions/SwgChatboxModel"
                status:
                  type: boolean
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"
  PopulatePath:
    x-swagger-router-controller: swg-chatbox-model
    post:
      operationId: chatboxPopulate
      description: |
        populate fake data for the chatbox model
      tags:
        - Account
      security:
        - $ref: ../swg-account-security-model/swagger.yaml#/definitions/OperationSecurityItem
      responses:
        201:
          description: Success
          schema:
            $ref: "#/definitions/SuccessResponse"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"
  SuccessResponse:
    required:
      - message
    properties:
      message:
        type: string
  ErrorResponse:
    required:
      - message
    properties:
      message:
        type: string
  AddParameter:
    type: object
    required:
      - name
      - description
    properties:
      name:
        type: string
      description:
        type: string
  UpdateParameter:
    type: object
    properties:
      name:
        type: string
      description:
        type: string

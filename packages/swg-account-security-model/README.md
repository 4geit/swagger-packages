# @4geit/swg-account-security-model [![npm version](//badge.fury.io/js/@4geit%2Fswg-account-security-model.svg)](//badge.fury.io/js/@4geit%2Fswg-account-security-model)

---

security definition for account api

## Installation

1. A recommended way to install ***@4geit/swg-account-security-model*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-account-security-model) package manager using the following command:

```bash
npm i @4geit/swg-account-security-model --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-account-security-model
```

2. In your swagger file, you need to add a reference to the `SwgAccountSecurityModel` definition under the `definitions` property (e.g. `/api/swagger/swagger.yaml`) as below:

```yaml
swagger: "2.0"
# ...
definitions:
  SwgAccountSecurityModel:
    $ref: ../../node_modules/@4geit/swg-account-security-model/swagger.yaml#/definitions/SwgAccountSecurityModel
# ...
```

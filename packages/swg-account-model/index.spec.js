'use strict'

const expect = require('chai').expect
const request = require('supertest')
const Swagmock = require('swagmock')
const MongoClient = require('mongodb').MongoClient
const swgDereferenceSwaggerHelper = require('@4geit/swg-dereference-swagger-helper')
const _app = require('../../app')

describe('swg-account-model', () => {
  // build swagmock object
  before(async () => {
    const { resolved } = await swgDereferenceSwaggerHelper()
    this.mockgen = Swagmock(resolved);
    this.app = await _app
  })
  beforeEach(async () => {
    this.app.locals.db = await MongoClient.connect('mongodb://db/test')
  })
  afterEach(async () => {
    await this.app.locals.db.dropDatabase()
  })
  // users endpoint
  describe('users', () => {
    const path = '/users'
    const operation = 'get'

    it('has to contains the correct parameters', async () => {
      const { parameters } = await this.mockgen.parameters({
        path,
        operation,
      })
      expect(parameters).to.not.be.undefined
      expect(Object.keys(parameters)).to.deep.equal([
        'query',
      ])
      const { query } = parameters
      expect(query).to.not.be.undefined
      expect(query.map(({ name }) => name)).to.deep.equal([
        'page',
        'pageSize'
      ])
    })
    it('has to contains the correct response fields', async () => {
      const { responses } = await this.mockgen.responses({
        path,
        operation,
        response: 200,
      })
      expect(responses).to.not.be.undefined
      expect(responses).to.have.lengthOf.at.least(1)
      const [ response ] = responses
      expect(Object.keys(response)).to.deep.equal([
        'id',
        'email',
        'password',
        'token',
        'tokens',
        'roles',
        'firstname',
        'lastname',
        'company',
        'address',
        'phone',
        'active',
        'diff1',
        'diff2',
        'diff3',
      ])
    })
    it('returns status code 200', async () => {
      const response = await request(this.app).get('/v1/users')
      expect(response.statusCode).to.equal(200)
    })
    it('returns an empty array', async () => {
      const response = await request(this.app).get('/v1/users')
      expect(response.body).to.deep.equal([])
    })
  })
  // populate endpoint
  describe('populate', () => {
    const path = '/users/populate'
    const operation = 'post'

    it('has to contains no parameters', async () => {
      const { parameters } = await this.mockgen.parameters({
        path,
        operation,
      })
      expect(parameters).to.deep.equal({})
    })
    it('has to contains the correct response fields', async () => {
      const { responses } = await this.mockgen.responses({
        path,
        operation,
        response: 201,
      })
      expect(responses).to.not.be.undefined
      expect(Object.keys(responses)).to.deep.equal([
        'message',
      ])
    })
  })
})

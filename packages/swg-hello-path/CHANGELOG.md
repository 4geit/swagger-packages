# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.31.0"></a>
# [1.31.0](https://gitlab.com/4geit/swagger-packages/compare/v1.30.2...v1.31.0) (2017-10-22)


### Features

* **test environment:** setup the new test environment ([5f492cb](https://gitlab.com/4geit/swagger-packages/commit/5f492cb))




<a name="1.28.0"></a>
# [1.28.0](https://gitlab.com/4geit/swagger-packages/compare/v1.27.0...v1.28.0) (2017-10-06)




**Note:** Version bump only for package @4geit/swg-hello-path

<a name="1.19.1"></a>
## [1.19.1](https://gitlab.com/4geit/swagger-packages/compare/v1.19.0...v1.19.1) (2017-09-26)


### Bug Fixes

* **hello:** add deps ([c60e3bc](https://gitlab.com/4geit/swagger-packages/commit/c60e3bc))




<a name="1.15.1"></a>
## [1.15.1](https://gitlab.com/4geit/swagger-packages/compare/v1.15.0...v1.15.1) (2017-09-23)




**Note:** Version bump only for package @4geit/swg-hello-path

<a name="1.10.1"></a>
## [1.10.1](https://gitlab.com/4geit/swagger-packages/compare/v1.10.0...v1.10.1) (2017-09-18)




**Note:** Version bump only for package @4geit/swg-hello-path

<a name="1.5.0"></a>
# [1.5.0](https://gitlab.com/4geit/swagger-packages/compare/v1.4.0...v1.5.0) (2017-08-27)


### Features

* **packages:** import all other swagger packages ([0610f0a](https://gitlab.com/4geit/swagger-packages/commit/0610f0a))

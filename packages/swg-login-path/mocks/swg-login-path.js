'use strict'

const swgCheckAvailabilityHelper = require('@4geit/swg-check-availability-helper')
const swgGetUserWithCredentialsHelper = require('@4geit/swg-get-user-with-credentials-helper')
const swgUseDbHelper = require('@4geit/swg-use-db-helper')
const swgDestructuringParametersHelper = require('@4geit/swg-destructuring-parameters-helper')

module.exports = {
  login: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithCredentialsHelper(
          ({ req, res, user }) => {
            return res.status(201).json(user)
          }
        )
      )
    )
  )
}

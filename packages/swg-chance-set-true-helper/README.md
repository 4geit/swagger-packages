# @4geit/swg-chance-set-true-helper [![npm version](//badge.fury.io/js/@4geit%2Fswg-chance-set-true-helper.svg)](//badge.fury.io/js/@4geit%2Fswg-chance-set-true-helper)

---

chance mixin to set a property value to true

## Installation

1. A recommended way to install ***@4geit/swg-chance-set-true-helper*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-chance-set-true-helper) package manager using the following command:

```bash
npm i @4geit/swg-chance-set-true-helper --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-chance-set-true-helper
```

2. All you have to do is to import the `@4geit/swg-chance-set-true-helper` package in your controller or mock file as below:

```js
// ...
const swgChanceSetTrueHelper = require('@4geit/swg-chance-set-true-helper');
// ...
```

And use it with one of your endpoint controller as below:

```js
// ...
module.exports = {
  // ...
  getItems: swgChanceSetTrueHelper(
    (req, res) => {
      // ...
    }
  ),
  // ...
};
```

FROM registry.gitlab.com/4geit/docker/lerna

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . /usr/src/app
RUN yarn

# @4geit/swg-product-model [![npm version](//badge.fury.io/js/@4geit%2Fswg-product-model.svg)](//badge.fury.io/js/@4geit%2Fswg-product-model)

---

product model and endpoints

## Installation

1. A recommended way to install ***@4geit/swg-product-model*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-product-model) package manager using the following command:

```bash
npm i @4geit/swg-product-model --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-product-model
```

2. In your swagger file, you need to add a reference to the `SwgProductModel` definition under the `definitions` property (e.g. `/api/swagger/swagger.yaml`) as below:

```yaml
swagger: "2.0"
# ...
definitions:
  SwgProductModel:
    $ref: ../../node_modules/@4geit/swg-product-model/swagger.yaml#/definitions/SwgProductModel
# ...
```

3. SwgProductModel also comes along with some endpoints you can expose to the API, to do so you will need to add a reference to the `SwgProductModel` definition under the `paths` property (e.g. `/api/swagger/swagger.yaml`) as below:

```yaml
swagger: "2.0"
# ...
paths:
  /product:
    $ref: ../../node_modules/@4geit/swg-product-model/swagger.yaml#/definitions/ProductListPath
  /product/{id}:
    $ref: ../../node_modules/@4geit/swg-product-model/swagger.yaml#/definitions/ProductItemPath
  /product/populate:
    $ref: ../../node_modules/@4geit/swg-product-model/swagger.yaml#/definitions/PopulatePath
# ...
```

And you will also need to add the path to the `controllers` folder of the `swg-product-model` package so that swagger-node will find the relevant controller to use. Edit the file `/config/default.yaml` and add two new paths to the properties `mockControllersDirs` and `controllersDirs` as illustrated below:

```yaml
swagger:
  # ...
  bagpipes:
    _router:
      # ...
      mockControllersDirs:
        # ...
        - node_modules/@4geit/swg-product-model/mocks
        # ...
      controllersDirs:
        # ...
        - node_modules/@4geit/swg-product-model/controllers
        # ...
```

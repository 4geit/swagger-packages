'use strict'

const jsf = require('json-schema-faker')
const faker = require('faker')
faker.seed(24)
const Chance = require('chance')
const chance = new Chance(24)
const path = require('path')

const swgCheckAvailabilityHelper = require('@4geit/swg-check-availability-helper')
const swgUseDbHelper = require('@4geit/swg-use-db-helper')
const swgDereferenceSwaggerHelper = require('@4geit/swg-dereference-swagger-helper')
const swgGetUserWithApiTokenHelper = require('@4geit/swg-get-user-with-api-token-helper');
const swgDestructuringParametersHelper = require('@4geit/swg-destructuring-parameters-helper')
const swgPaginateHelper = require('@4geit/swg-paginate-helper')

module.exports = {
  contactList: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          swgPaginateHelper(
            async ({ req, res, db, page, pageSize, user }) => {
              try {
                const firstname = req.swagger.params.firstname.value
                const lastname = req.swagger.params.lastname.value
                // define db collection to use
                const collection = db.collection('contact')
                // get counter
                const counter = await collection.count({
                  user_id: user._id,
                  firstname: {
                    $regex: firstname || '',
                    $options: 'i',
                  },
                  lastname: {
                    $regex: lastname || '',
                    $options: 'i',
                  },
                })
                res.append('X-TotalCount', counter)
                // get all items
                const docs = await collection.find({
                  user_id: user._id,
                  firstname: {
                    $regex: firstname || '',
                    $options: 'i',
                  },
                  lastname: {
                    $regex: lastname || '',
                    $options: 'i',
                  },
                }).skip(pageSize * (page - 1)).limit(pageSize).toArray()
                // embeed
                const resolution = await Promise.all(docs.map(async ({ /*other_id,*/ user_id, ...fields }) => ({
                  ...fields,
                  user,
                  // other: await others.findOne({ _id: other_id }),
                })))
                // return result
                res.json(resolution)
              } catch (err) {
                console.log(err)
                res.status(500).json({
                  message: err
                })
              }
            }
          )
        )
      )
    )
  ),
  contactAdd: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const { _id, id, user_id, /*otherId,*/ ...fields } = req.swagger.params.body.value
              // define db collection to use
              // const otherCollection = db.collection('others')
              const collection = db.collection('contact')
              // find other
              // const otherItem = await otherCollection.findOne({
              //   id: otherId,
              // })
              // if (!otherItem) {
              //   // send an error object if otherItem is nil
              //   return res.status(403).json({
              //     message: 'Invalid other ID.'
              //   })
              // }
              // add the new contact to the collection
              await collection.insert({
                id: chance.guid(),
                user_id: user._id,
                // other_id: other._id,
                ...fields,
              })
              // return message
              return res.status(201).json({
                message: 'added contact item!'
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  contactBulkAdd: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const body = req.swagger.params.body.value
              // define db collection to use
              // const otherCollection = db.collection('others')
              const collection = db.collection('contact')
              // loop over bulk items
              await Promise.all(body.map(async ({ _id, id, user_id, /*otherId,*/ ...fields }) => {
                // find other
                // const otherItem = await otherCollection.findOne({
                //   id: otherId,
                // })
                // if (!otherItem) {
                //   // send an error object if otherItem is nil
                //   return res.status(403).json({
                //     message: 'Invalid other ID.'
                //   })
                // }
                // add the new contact to the collection
                await collection.insert({
                  id: chance.guid(),
                  user_id: user._id,
                  // other_id: other._id,
                  ...fields,
                })
              }))
              // return message
              return res.status(201).json({
                message: 'added contact items!'
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  contactGet: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const id = req.swagger.params.id.value
              // define db collection to use
              // const otherCollection = db.collection('others')
              const collection = db.collection('contact')
              // find item
              const item = await collection.findOne({
                id,
                user_id: user._id
              })
              if (!item) {
                // send an error object if item is nil
                return res.status(403).json({
                  message: 'Invalid contact ID.'
                })
              }
              // embeed
              const { /*other_id,*/ user_id, ...fields } = item
              const resolution = {
                ...fields,
                user,
                // other: await otherCollection.findOne({ _id: other_id }),
              }
              // return item
              return res.status(200).json(resolution)
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  contactDelete: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const id = req.swagger.params.id.value
              // define db collection to use
              const collection = db.collection('contact')
              // delete item
              await collection.deleteOne({
                id,
                user_id: user._id
              })
              // return message
              return res.status(200).json({
                message: 'contact deleted!'
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  contactUpdate: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const itemId = req.swagger.params.id.value
              const { _id, id, user_id, /*other_id,*/ ...fields } = req.swagger.params.body.value
              // define db collection to use
              const collection = db.collection('contact')
              // udptae item
              if (!Object.keys(fields).length) {
                // send an error object if update failed
                return res.status(500).json({
                  message: 'No field to update.'
                })
              }
              const result = await collection.updateOne({
                id: itemId,
                user_id: user._id,
              }, { $set: { ...fields } })
              if (!result.modifiedCount) {
                // send an error object if update failed
                return res.status(500).json({
                  message: 'Update failed.'
                })
              }
              // return item
              return res.status(200).json({
                message: 'Update succeeded.'
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  contactPopulate: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              // define db collection to use
              const collection = db.collection('contact')
              // resolve swagger
              const { resolved: { definitions: { SwgContactModel } } } = await swgDereferenceSwaggerHelper(path.resolve(__dirname, '../swagger.yaml'))
              // add custom faker generator
              jsf.extend('faker', () => {
                faker.custom = {}
                return faker
              })
              // add custom chance generator
              jsf.extend('chance', () => {
                const custom = {
                  item: () => jsf(SwgContactModel),
                  setTrue: () => true,
                  setFalse: () => false,
                  tz: () => chance.timezone().abbr,
                }
                chance.mixin(custom)
                return chance
              })
              // create fake items and insert them to db
              await collection.insertMany(
                chance.n(chance.item, 10).map(({ user: dummyUser, ...fields }) => ({ user_id: user._id, ...fields }))
              )
              // return success message
              res.status(201).json({
                message: "Succeeded to populate fake contact data to the db."
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  )
}

#!/bin/bash

function prompt {
  if [ "$1" == "" ]; then
    echo "Type the package name"
    read NAME
  else
    NAME=$1
  fi

  if [ "$2" == "" ]; then
    echo "Type the package type <path|operation|definition|model|helper>"
    read TYPE
  else
    TYPE=$2
  fi

  if [ "$AUTHOR_NAME" == "" ]; then
    if [ "$3" == "" ]; then
      echo "Type author name"
      read AUTHOR_NAME
    else
      AUTHOR_NAME=$3
    fi
  fi

  if [ "$AUTHOR_EMAIL" == "" ]; then
    if [ "$4" == "" ]; then
      echo "Type author email"
      read AUTHOR_EMAIL
    else
      AUTHOR_EMAIL=$4
    fi
  fi

  echo "Type the package description"
  read DESCRIPTION
}
function set_variables {
  FILE=$1
  CLASS=$(echo swg-$NAME-$TYPE | sed -r 's/(^|-)([a-z])/\U\2/g')
  FUNCTION=$(echo ${CLASS,})
  NAME_CLASS=$(echo $NAME | sed -r 's/(^|-)([a-z])/\U\2/g')
  NAME_FUNCTION=$(echo ${NAME_CLASS,})
  sed -i \
    -e "s/<NAME>/$NAME/g" \
    -e "s/<TYPE>/$TYPE/g" \
    -e "s/<AUTHOR_NAME>/$AUTHOR_NAME/g" \
    -e "s/<AUTHOR_EMAIL>/$AUTHOR_EMAIL/g" \
    -e "s/<DESCRIPTION>/$DESCRIPTION/g" \
    -e "s/<CLASS>/$CLASS/g" \
    -e "s/<FUNCTION>/$FUNCTION/g" \
    -e "s/<NAME_CLASS>/$NAME_CLASS/g" \
    -e "s/<NAME_FUNCTION>/$NAME_FUNCTION/g" \
    ./packages/swg-$NAME-$TYPE/$FILE
}
function create_folders {
  for folder in \
    "swg-$NAME-$TYPE" \
  ; do
    echo "create the folder $folder"
    mkdir -p packages/$folder
  done
  if [ "$TYPE" == "path" ] || [ "$TYPE" == "operation" ] || [ "$TYPE" == "model" ]; then
    for folder in \
      "swg-$NAME-$TYPE/controllers" \
      "swg-$NAME-$TYPE/mocks" \
    ; do
      echo "create the folder $folder"
      mkdir -p packages/$folder
    done
  fi
}
function simple_copies {
  for file in \
    "package.json" \
  ; do
    echo "copy $file"
    cp ./scripts/package-template/$file ./packages/swg-$NAME-$TYPE/$file
    set_variables $file
  done
}
function simple_type_copies {
  for file in \
    "README.md" \
  ; do
    echo "copy $file"
    cp ./scripts/package-template/$file.$TYPE ./packages/swg-$NAME-$TYPE/$file
    set_variables $file
  done
  if [ "$TYPE" != "helper" ]; then
    for file in \
      "swagger.yaml" \
    ; do
      echo "copy $file"
      cp ./scripts/package-template/$file.$TYPE ./packages/swg-$NAME-$TYPE/$file
      set_variables $file
    done
  fi
  if [ "$TYPE" == "path" ] || [ "$TYPE" == "operation" ] || [ "$TYPE" == "model" ] ; then
    for file in \
      "index.spec.js" \
    ; do
      echo "copy $file"
      cp ./scripts/package-template/$file.$TYPE ./packages/swg-$NAME-$TYPE/$file
      set_variables $file
    done
  fi
  if [ "$TYPE" == "helper" ]; then
    for file in \
      "index.js" \
      "index.spec.js" \
    ; do
      echo "copy $file"
      cp ./scripts/package-template/$file.$TYPE ./packages/swg-$NAME-$TYPE/$file
      set_variables $file
    done
  fi
  if [ "$TYPE" == "path" ] || [ "$TYPE" == "operation" ] || [ "$TYPE" == "model" ]; then
    echo "create the file ./controllers/swg-$NAME-$TYPE.js"
    cp ./scripts/package-template/index.js.$TYPE ./packages/swg-$NAME-$TYPE/controllers/swg-$NAME-$TYPE.js
    set_variables controllers/swg-$NAME-$TYPE.js

    echo "create the file ./mocks/swg-$NAME-$TYPE.js"
    cp ./scripts/package-template/index.js.$TYPE ./packages/swg-$NAME-$TYPE/mocks/swg-$NAME-$TYPE.js
    set_variables mocks/swg-$NAME-$TYPE.js
  fi
}
function run_yarn {
  echo "link packages to each other (yarn)"
  yarn
}
function usage {
  echo
  echo
  echo "The new package has been properly generated and is available under the packages folder."
  echo
  echo "You can get access to the generated package with the following command:"
  echo
  echo "cd packages/swg-$NAME-$TYPE"
  echo
}

prompt $*
create_folders
simple_copies
simple_type_copies
run_yarn
usage

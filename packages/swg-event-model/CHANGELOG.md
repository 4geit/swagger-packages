# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.31.1"></a>
## [1.31.1](https://gitlab.com/4geit/swagger-packages/compare/v1.31.0...v1.31.1) (2017-10-25)




**Note:** Version bump only for package @4geit/swg-event-model

<a name="1.31.0"></a>
# [1.31.0](https://gitlab.com/4geit/swagger-packages/compare/v1.30.2...v1.31.0) (2017-10-22)


### Features

* **jest:** convert to jest test environment ([95b423f](https://gitlab.com/4geit/swagger-packages/commit/95b423f))




<a name="1.28.0"></a>
# [1.28.0](https://gitlab.com/4geit/swagger-packages/compare/v1.27.0...v1.28.0) (2017-10-06)




**Note:** Version bump only for package @4geit/swg-event-model

<a name="1.15.1"></a>
## [1.15.1](https://gitlab.com/4geit/swagger-packages/compare/v1.15.0...v1.15.1) (2017-09-23)




**Note:** Version bump only for package @4geit/swg-event-model

<a name="1.10.1"></a>
## [1.10.1](https://gitlab.com/4geit/swagger-packages/compare/v1.10.0...v1.10.1) (2017-09-18)




**Note:** Version bump only for package @4geit/swg-event-model

<a name="1.10.0"></a>
# [1.10.0](https://gitlab.com/4geit/swagger-packages/compare/v1.9.0...v1.10.0) (2017-09-18)


### Features

* **user-chatbox-model:** add listing + populate endpoints ([1f307c1](https://gitlab.com/4geit/swagger-packages/commit/1f307c1))




<a name="1.8.0"></a>
# [1.8.0](https://gitlab.com/4geit/swagger-packages/compare/v1.7.6...v1.8.0) (2017-09-17)


### Features

* **event-model:** add new event-model + endpoints ([b9b65ad](https://gitlab.com/4geit/swagger-packages/commit/b9b65ad))

'use strict'

const fs = require('fs')

module.exports = (filename, library, generator, n) => {
  // build path
  const path = `./api/mocks/data/${filename}.json`
  // create the data and save to a file
  if (!fs.existsSync(path)) {
    const data = library(generator, n)
    // write the data to a file
    fs.writeFileSync(path, JSON.stringify(data, null, '  '), 'utf8')
    return data
  }
  // already exist, load the data from its file
  return JSON.parse( fs.readFileSync(path) )
}

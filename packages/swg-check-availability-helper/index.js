'use strict'

module.exports = cb => {
  return ({ req, res, ...fields }) => {
    if ('x-disabled' in req.swagger.operation && req.swagger.operation['x-disabled']) {
      res.status(410).json({
        message: 'Endpoint disabled.'
      })
      return
    }
    return cb({ req, res, ...fields })
  }
}

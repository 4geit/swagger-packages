'use strict'

module.exports = cb => {
  return async ({ req, res, db, ...fields }) => {
    const account = req.swagger.params.account.value
    const { email, password } = account
    if (account === undefined || email === undefined || password === undefined) {
      // no credentials passed
      return res.status(401).json({
        message: 'No credentials.'
      })
    }

    // define db collection to use
    const users = db.collection('users')

    const user = await users.findOne({
      email,
      password
    })
    if (!user) {
      // send an error object if user is nil
      return res.status(403).json({
        message: 'Invalid login.'
      })
    }
    // if found a matching user, send it
    return cb({ req, res, db, user, ...fields })
  }
}

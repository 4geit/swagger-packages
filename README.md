# Swagger Packages

---

This repository is a [monorepo](http://www.drmaciver.com/2016/10/why-you-should-use-a-single-repository-for-all-your-companys-projects/) intended to host reusable Swagger path, operation, definition, model or helper thanks to [Lerna](https://github.com/lerna/lerna) and [Yarn Workspaces](https://yarnpkg.com/blog/2017/08/02/introducing-workspaces/).

A live test API is available to test the endpoints @ http://swagger-packages-mock.vc1s.4ge.it as well as its [swagger specification](http://swagger-packages-mock.vc1s.4ge.it/v1/swagger).

You can find all the Swagger packages under the `packages` folder.

## Installation

1. This monorepo requires `yarn` version `>=1.0.0` to work properly. If you have a previous version you can use the following command line to get it upgraded:

```bash
sudo npm i -g yarn
```

2. A recommended way to install ***@4geit/swagger-packages*** is through [git](//gitlab.com/4geit/swagger-packages) repository using the following command:

```bash
git clone git@gitlab.com:4geit/swagger-packages.git
```

alternatively you can use the HTTPS URL:

```bash
git clone https://gitlab.com/4geit/swagger-packages.git
```

And finally change your directory to the folder `swagger-packages`:

```bash
cd swagger-packages
```

3. Now you are ready to install all the packages dependencies by using simply the command line:

```bash
yarn
```

4. (Optional), you can also skip typing the author name and email each time that you want to create a package/project by adding those lines in your `~/.bashrc` file:

```bash
export AUTHOR_NAME="John Doe"
export AUTHOR_EMAIL="john.doe@provider.xyz"
```

* `<AUTHOR_NAME>`: the name of the author stored in the `package.json` generated when creating a new package/project.
* `<AUTHOR_EMAIL>`: the name of the author stored in the `package.json` generated when creating a new package/project.

## Package generator

### Create a new package

1. The easiest way to generate a new repository with `swagger-packages` is to use the following command line:

```bash
yarn create-new-package
```

The command will ask a set of values such as:

* `<NAME>`: the name of the repository the one used in `swg-<NAME>-<TYPE>`
* `<TYPE>`: the type of the repository the one used in `swg-<NAME>-<TYPE>` it can contain the value `path`, `operation`, `definition`, `model` or `helper`.
* `<AUTHOR_NAME>`: the name of the author stored in the `package.json`
* `<AUTHOR_EMAIL>`: the name of the author stored in the `package.json`
* `<DESCRIPTION>`: the description of the project stored in the `package.json` and `README.md`

2. But you can also use the command argument by doing so:

```
yarn create-new-package <NAME> <TYPE> <AUTHOR_NAME> <AUTHOR_EMAIL>
```

You will still be asked to type the description though.

3. Once the package is generated, you will see it available under the folder `./packages`.

4. You are now ready to use your new package by reading its `README.md` file.

### Create a new project

1. You will need to add those two additional environment variables in your `~/.bashrc` file:

```bash
export GITLAB_PRIVATE_TOKEN=YOUR_GITLAB_PRIVATE_TOKEN
export RUNNER_ID=YOUR_RUNNER_ID
```

* `GITLAB_PRIVATE_TOKEN`: a gitlab token allowed to create a project under the group `4geit`. You can get your private gitlab token [right here](https://gitlab.com/profile/account) under the `Private token` section.
* `RUNNER_ID`: the runner id that will run the CI build. Ask your team in order to provide you this ID.

Plus, you will also need to install the tool `jq` if it's not installed yet, assuming you are using `Debian` and `Ubuntu`, you can use the following command line:

```bash
sudo apt install jq
```

2. The easiest way to generate a new project with `swagger-packages` is to use the following command line:

```bash
yarn create-new-project
```

* `<GROUP>`: the group repository where it will be located on gitlab.com, the one used in `<GROUP>/<NAME>`
* `<NAME>`: the name of the repository the one used in `<GROUP>/<NAME>`
* `<DESCRIPTION>`: the description of the project stored in the `package.json` and `README.md`

3. But you can also use the command argument by doing so:

```bash
yarn create-new-project <GROUP> <NAME>
```

You will still be asked to type the description though.

4. You will need to run the `yarn` command in order to install its dependencies.

## Swagger-Node

`swagger-packages` comes along with a test api you can run with the following command line:

```bash
yarn mock
```

And the server will be running locally at http://localhost:10010

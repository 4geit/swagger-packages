# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.10.1"></a>
## [1.10.1](https://gitlab.com/4geit/swagger-packages/compare/v1.10.0...v1.10.1) (2017-09-18)




**Note:** Version bump only for package @4geit/swg-chance-set-false-helper

<a name="1.5.0"></a>
# [1.5.0](https://gitlab.com/4geit/swagger-packages/compare/v1.4.0...v1.5.0) (2017-08-27)


### Features

* **packages:** import all other swagger packages ([0610f0a](https://gitlab.com/4geit/swagger-packages/commit/0610f0a))

const app = require('./app')

const port = process.env.PORT || 10010

app
  .then(_app => _app.listen(port))
  .catch(err => console.error(err))

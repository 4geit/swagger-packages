'use strict'

const jsf = require('json-schema-faker')
const faker = require('faker')
faker.seed(24)
const Chance = require('chance')
const chance = new Chance(24)
const path = require('path')

const swgCheckAvailabilityHelper = require('@4geit/swg-check-availability-helper')
const swgUseDbHelper = require('@4geit/swg-use-db-helper')
const swgDereferenceSwaggerHelper = require('@4geit/swg-dereference-swagger-helper')
const swgDestructuringParametersHelper = require('@4geit/swg-destructuring-parameters-helper')

module.exports = {
  eventList: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        async ({ req, res, db }) => {
          // define db collection to use
          const events = db.collection('events')
          // get all events
          const docs = await events.find().toArray()
          // return result
          res.json(docs)
        }
      )
    )
  ),
  populate: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        async ({ req, res, db }) => {
          try {
            // define db collection to use
            const events = db.collection('events')
            // resolve swagger
            const { resolved: { definitions: { SwgEventModel } } } = await swgDereferenceSwaggerHelper(path.resolve(__dirname, '../swagger.yaml'))
            // add custom faker generator
            jsf.extend('faker', () => {
              faker.custom = {}
              return faker
            })
            // add custom chance generator
            jsf.extend('chance', () => {
              const custom = {
                event: () => jsf(SwgEventModel),
                setTrue: () => true,
                setFalse: () => false,
                tz: () => chance.timezone().abbr,
              }
              chance.mixin(custom)
              return chance
            })
            // create fake events and insert them to db
            await events.insertMany(
              chance.n(chance.event, 10)
            )
            // return success message
            res.status(201).json({
              message: "Succeeded to populate fake event data to the db."
            })
          } catch (err) {
            console.log(err)
            res.status(500).json({
              message: err
            })
          }
        }
      )
    )
  )
}

# @4geit/swg-dereference-swagger-helper [![npm version](//badge.fury.io/js/@4geit%2Fswg-dereference-swagger-helper.svg)](//badge.fury.io/js/@4geit%2Fswg-dereference-swagger-helper)

---

dereference swagger file and returns a promise with the plain swagger as js object

## Installation

1. A recommended way to install ***@4geit/swg-dereference-swagger-helper*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-dereference-swagger-helper) package manager using the following command:

```bash
npm i @4geit/swg-dereference-swagger-helper --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-dereference-swagger-helper
```

2. All you have to do is to import the `@4geit/swg-dereference-swagger-helper` package in your controller or mock file as below:

```js
// ...
const swgDereferenceSwaggerHelper = require('@4geit/swg-dereference-swagger-helper')
// ...
```

And use it with one of your endpoint controller as below:

```js
// ...
// resolve swagger
const { resolved: { definitions: { SwgSomeModel } } } = await swgDereferenceSwaggerHelper(path.resolve(__dirname, '../swagger.yaml'))
// ...
```

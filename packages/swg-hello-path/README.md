# @4geit/swg-hello-path [![npm version](//badge.fury.io/js/@4geit%2Fswg-hello-path.svg)](//badge.fury.io/js/@4geit%2Fswg-hello-path)

---

add a dummy path that enables an hello world endpoint in your api

## Installation

1. A recommended way to install ***@4geit/swg-hello-path*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-hello-path) package manager using the following command:

```bash
npm i @4geit/swg-hello-path --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-hello-path
```

2. In your swagger file, you need to add a reference to the `SwgHelloPath` definition under the `paths` property (e.g. `/api/swagger/swagger.yaml`) as below:

```yaml
swagger: "2.0"
# ...
paths:
  /hello:
    $ref: ../../node_modules/@4geit/swg-hello-path/swagger.yaml#/definitions/SwgHelloPath
# ...
```

And you will also need to add the path to the `controllers` folder of the `swg-hello-path` package so that swagger-node will find the relevant controller to use. Edit the file `/config/default.yaml` and add two new paths to the properties `mockControllersDirs` and `controllersDirs` as illustrated below:

```yaml
swagger:
  # ...
  bagpipes:
    _router:
      # ...
      mockControllersDirs:
        # ...
        - node_modules/@4geit/swg-hello-path/mocks
        # ...
      controllersDirs:
        # ...
        - node_modules/@4geit/swg-hello-path/controllers
        # ...
```

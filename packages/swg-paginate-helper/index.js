'use strict'

module.exports = cb => {
  return async ({ req, res, ...fields }) => {
    var page = req.swagger.params.page.value || 1
    var pageSize = req.swagger.params.pageSize.value || 100
    res.append('X-Page', page)
    res.append('X-PerPage', pageSize)
    res.append('Access-Control-Expose-Headers', 'X-Page, X-PerPage, X-TotalCount, X-PartlyInsuredCustomersCount')
    return cb({ req, res, page, pageSize, ...fields })
  }
}

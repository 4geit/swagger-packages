'use strict'

const SwaggerExpress = require('swagger-express-mw')
const app = require('express')()
const MongoClient = require('mongodb').MongoClient

const swgAllowCrossDomainHelper = require('@4geit/swg-allow-cross-domain-helper')
const swgDereferenceSwaggerHelper = require('@4geit/swg-dereference-swagger-helper')

function create(config) {
  return new Promise((resolve, reject) => {
    SwaggerExpress.create(config, (err, swaggerExpress) => {
      if (err) { reject(err) }
      resolve({ swaggerExpress })
    })
  })
}

module.exports = new Promise(async (resolve, reject) => {
  try {
    // dereference swagger
    const results = await swgDereferenceSwaggerHelper()
    // create swagger express object
    const { swaggerExpress } = await create({
      appRoot: __dirname, // required config
      swagger: results.resolved, // use the dereferenced swagger
      swaggerSecurityHandlers: {
        AccountSecurity: function (req, authOrSecDef, scopesOrApiKey, cb) {
          if (req.headers.token) {
            cb()
          } else {
            cb(new Error('Authorization has been denied for this request. (token is missing)'))
          }
        }
      }
    })
    // know if mocked
    app.locals.mockMode = swaggerExpress.runner.config.swagger.mockMode
    // connect db
    app.locals.db = await MongoClient.connect(`mongodb://db/${app.locals.mockMode ? 'mock' : 'database'}`)

    // connect middleware and handle mock mode
    // install middleware
    app.use(swaggerExpress.runner.swaggerTools.swaggerUi())
    // add cors headers
    app.use(swgAllowCrossDomainHelper)

    // register app and run server
    // install middleware
    swaggerExpress.register(app)

    var port = process.env.PORT || 10010

    if (swaggerExpress.runner.swagger.paths['/hello']) {
      console.log('try this:\ncurl http://127.0.0.1:' + port + '/v1/hello?name=Scott')
    }

    resolve(app)
  } catch (err) {
    // throw errors and stop the app
    console.error(err.stack)
    app.locals.db.close()
    reject(err)
  }
})

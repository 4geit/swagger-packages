const jsf = require('json-schema-faker')
const faker = require('faker')
faker.seed(24)
const Chance = require('chance')
const chance = new Chance(24)
const path = require('path')
const debug = require('debug')('swagger-packages:packages:swg-user-chatbox-model:mocks')

const swgCheckAvailabilityHelper = require('@4geit/swg-check-availability-helper')
const swgUseDbHelper = require('@4geit/swg-use-db-helper')
const swgDereferenceSwaggerHelper = require('@4geit/swg-dereference-swagger-helper')
const swgGetUserWithApiTokenHelper = require('@4geit/swg-get-user-with-api-token-helper')
const swgDestructuringParametersHelper = require('@4geit/swg-destructuring-parameters-helper')

module.exports = {
  userChatboxList: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const maximized = req.swagger.params.maximized.value
              // define db collection to use
              const messageCollection = db.collection('message')
              const chatboxCollection = db.collection('chatbox')
              const collection = db.collection('user-chatbox')
              // get all user chatboxes
              const addObjParams = {}
              if (maximized) {
                addObjParams.maximized = maximized
              }
              const docs = await collection.find({
                user_id: user._id,
                ...addObjParams,
              }).toArray()
              // embeed chatbox and user
              const resolution = await Promise.all(docs.map(async ({ chatbox_id, user_id, ...fields }) => ({
                ...fields,
                user,
                chatbox: await chatboxCollection.findOne({ _id: chatbox_id }),
                messages: await messageCollection.find({ chatbox_id, user_id, }).toArray(),
              })))
              // return result
              res.json(resolution)
            } catch (err) {
              debug(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  userChatboxAdd: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const { chatboxId } = req.swagger.params.body.value
              // define db collection to use
              const chatboxCollection = db.collection('chatbox')
              const collection = db.collection('user-chatbox')
              // find chatbox
              const chatbox = await chatboxCollection.findOne({
                id: chatboxId,
              })
              if (!chatbox) {
                // send an error object if chatbox is nil
                return res.status(403).json({
                  message: 'Invalid chatbox ID.'
                })
              }
              // add the new user chatbox to the collection
              await collection.insert({
                id: chance.guid(),
                user_id: user._id,
                chatbox_id: chatbox._id,
                position: 0,
                active: true,
                maximized: false,
              })
              // return message
              return res.status(201).json({
                message: 'Added user chatbox'
              })
            } catch (err) {
              debug(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  userChatboxBulkAdd: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const body = req.swagger.params.body.value
              // define db collection to use
              // const otherCollection = db.collection('others')
              const collection = db.collection('user-chatbox')
              // loop over bulk items
              // eslint-disable-next-line no-unused-vars
              await Promise.all(body.map(async ({ _id, id, user_id, /*otherId,*/ ...fields }) => {
                // find other
                // const otherItem = await otherCollection.findOne({
                //   id: otherId,
                // })
                // if (!otherItem) {
                //   // send an error object if otherItem is nil
                //   return res.status(403).json({
                //     message: 'Invalid other ID.'
                //   })
                // }
                // add the new <NAME> to the collection
                await collection.insert({
                  id: chance.guid(),
                  user_id: user._id,
                  // other_id: other._id,
                  ...fields,
                })
              }))
              // return message
              return res.status(201).json({
                message: 'added user-chatbox items!'
              })
            } catch (err) {
              debug(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  userChatboxGet: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const id = req.swagger.params.id.value
              // define db collection to use
              const messageCollection = db.collection('message')
              const chatboxCollection = db.collection('chatbox')
              const collection = db.collection('user-chatbox')
              // find chatbox
              const userChatbox = await collection.findOne({
                id,
                user_id: user._id,
              })
              if (!userChatbox) {
                // send an error object if chatbox is nil
                return res.status(403).json({
                  message: 'Invalid user chatbox ID.'
                })
              }
              // embeed chatbox and user
              const { chatbox_id, user_id, ...fields } = userChatbox
              const resolution = {
                ...fields,
                user,
                chatbox: await chatboxCollection.findOne({ _id: chatbox_id }),
                messages: await messageCollection.find({ chatbox_id, user_id, }).toArray(),
              }
              // return item
              return res.status(200).json(resolution)
            } catch (err) {
              debug(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  userChatboxDelete: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const id = req.swagger.params.id.value
              // define db collection to use
              const collection = db.collection('user-chatbox')
              // delete chatbox
              await collection.deleteOne({
                id,
                user_id: user._id,
              })
              // return item
              return res.status(200).json({
                message: 'user chatbox deleted!'
              })
            } catch (err) {
              debug(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  userChatboxUpdate: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            debug('userChatboxUpdate')
            try {
              const itemId = req.swagger.params.id.value
              // eslint-disable-next-line no-unused-vars
              const { _id, id, user_id, chatbox_id, ...fields } = req.swagger.params.body.value
              // define db collection to use
              const collection = db.collection('user-chatbox')
              // udptae item
              if (!Object.keys(fields).length) {
                // send an error object if update failed
                return res.status(500).json({
                  message: 'No field to update.'
                })
              }
              const result = await collection.updateOne({
                id: itemId,
                user_id: user._id,
              }, { $set: { ...fields } })
              if (!result.matchedCount) {
                // send an error object if update failed
                return res.status(500).json({
                  message: 'Update failed.'
                })
              }
              // return item
              return res.status(200).json({
                message: 'Update succeeded.'
              })
            } catch (err) {
              debug(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  userChatboxBulkUpdate: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            debug('userChatboxBulkUpdate')
            try {
              // eslint-disable-next-line no-unused-vars
              const { _id, id, user_id, /*other_id,*/ ...fields } = req.swagger.params.body.value
              debug(fields)
              // define db collection to use
              const collection = db.collection('user-chatbox')
              // check if there a field to update
              if (!Object.keys(fields).length) {
                // send an error if no fields are passed
                return res.status(500).json({
                  message: 'No field to update.'
                })
              }
              const result = await collection.updateMany({
                user_id: user._id,
              }, { $set: { ...fields } })
              debug(result)
              if (!result.matchedCount) {
                // send an error object if update failed
                return res.status(500).json({
                  message: 'Update failed.'
                })
              }
              // return item
              return res.status(200).json({
                message: 'Update succeeded.'
              })
            } catch (err) {
              debug(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  userChatboxPopulate: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          // eslint-disable-next-line no-unused-vars
          async ({ req, res, db, user }) => {
            try {
              // define db collection to use
              const collection = db.collection('user-chatbox')
              // resolve swagger
              const { resolved: { definitions: { SwgUserChatboxModel } } } = await swgDereferenceSwaggerHelper(path.resolve(__dirname, '../swagger.yaml'))
              // add custom faker generator
              jsf.extend('faker', () => {
                faker.custom = {}
                return faker
              })
              // add custom chance generator
              jsf.extend('chance', () => {
                const custom = {
                  userChatbox: () => jsf(SwgUserChatboxModel),
                  setTrue: () => true,
                  setFalse: () => false,
                }
                chance.mixin(custom)
                return chance
              })
              // create fake user chatbox and insert them to db
              await collection.insertMany(
                // eslint-disable-next-line no-unused-vars
                chance.n(chance.item, 10).map(({ user: dummyUser, ...fields }) => ({ user_id: user._id, ...fields }))
              )
              // return success message
              res.status(201).json({
                message: "Succeeded to populate fake user chatbox data to the db."
              })
            } catch (err) {
              debug(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  )
}

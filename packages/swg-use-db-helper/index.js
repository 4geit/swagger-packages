'use strict'

module.exports = cb => {
  return ({ req, res, ...fields }) => {
    const db = req.app.locals.db
    return cb({ req, res, db, ...fields })
  }
};

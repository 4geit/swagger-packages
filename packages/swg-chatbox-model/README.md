# @4geit/swg-chatbox-model [![npm version](//badge.fury.io/js/@4geit%2Fswg-chatbox-model.svg)](//badge.fury.io/js/@4geit%2Fswg-chatbox-model)

---

chatbox model for the chatbox feature using CRUD operators

## Installation

1. A recommended way to install ***@4geit/swg-chatbox-model*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-chatbox-model) package manager using the following command:

```bash
npm i @4geit/swg-chatbox-model --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-chatbox-model
```

2. In your swagger file, you need to add a reference to the `SwgChatboxModel` definition under the `definitions` property (e.g. `/api/swagger/swagger.yaml`) as below:

```yaml
swagger: "2.0"
# ...
definitions:
  SwgChatboxModel:
    $ref: ../../node_modules/@4geit/swg-chatbox-model/swagger.yaml#/definitions/SwgChatboxModel
# ...
```

3. SwgChatboxModel also comes along with some endpoints you can expose to the API, to do so you will need to add a reference to the `SwgChatboxModel` definition under the `paths` property (e.g. `/api/swagger/swagger.yaml`) as below:

```yaml
swagger: "2.0"
# ...
paths:
  /chatbox:
    $ref: ../../node_modules/@4geit/swg-chatbox-model/swagger.yaml#/definitions/ChatboxListPath
  /chatbox/{id}:
    $ref: ../../node_modules/@4geit/swg-chatbox-model/swagger.yaml#/definitions/ChatboxItemPath
  /chatbox/populate:
    $ref: ../../node_modules/@4geit/swg-chatbox-model/swagger.yaml#/definitions/PopulatePath
# ...
```

And you will also need to add the path to the `controllers` folder of the `swg-chatbox-model` package so that swagger-node will find the relevant controller to use. Edit the file `/config/default.yaml` and add two new paths to the properties `mockControllersDirs` and `controllersDirs` as illustrated below:

```yaml
swagger:
  # ...
  bagpipes:
    _router:
      # ...
      mockControllersDirs:
        # ...
        - node_modules/@4geit/swg-chatbox-model/mocks
        # ...
      controllersDirs:
        # ...
        - node_modules/@4geit/swg-chatbox-model/controllers
        # ...
```

# @4geit/swg-account-model [![npm version](//badge.fury.io/js/@4geit%2Fswg-account-model.svg)](//badge.fury.io/js/@4geit%2Fswg-account-model)

---

swagger definition of an account model following the REST CRUD pattern

## Installation

1. A recommended way to install ***@4geit/swg-account-model*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-account-model) package manager using the following command:

```bash
npm i @4geit/swg-account-model --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-account-model
```

2. In your swagger file, you need to add a reference to the `SwgAccountModel` definition under the `definitions` property (e.g. `/api/swagger/swagger.yaml`) as below:

```yaml
swagger: "2.0"
# ...
definitions:
  SwgAccountModel:
    $ref: ../../node_modules/@4geit/swg-account-model/swagger.yaml#/definitions/SwgAccountModel
# ...
```

3. In your swagger file, you need to add a reference to the `PopulatePath` definition under the `paths` property (e.g. `/api/swagger/swagger.yaml`) as below:

```yaml
swagger: "2.0"
# ...
paths:
  /populate:
    $ref: ../../node_modules/@4geit/swg-account-model/swagger.yaml#/definitions/PopulatePath
  /users:
    $ref: ../../node_modules/@4geit/swg-account-model/swagger.yaml#/definitions/UsersPath
# ...
```

And you will also need to add the path to the `controllers` folder of the `swg-account-model` package so that swagger-node will find the relevant controller to use. Edit the file `/config/default.yaml` and add two new paths to the properties `mockControllersDirs` and `controllersDirs` as illustrated below:

```yaml
swagger:
  # ...
  bagpipes:
    _router:
      # ...
      mockControllersDirs:
        # ...
        - node_modules/@4geit/swg-account-model/mocks
        # ...
      controllersDirs:
        # ...
        - node_modules/@4geit/swg-account-model/controllers
        # ...
```

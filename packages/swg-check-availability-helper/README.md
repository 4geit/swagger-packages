# @4geit/swg-check-availability-helper [![npm version](//badge.fury.io/js/@4geit%2Fswg-check-availability-helper.svg)](//badge.fury.io/js/@4geit%2Fswg-check-availability-helper)

---

wrapper function to disable a swagger endpoint if x-disabled property is true

## Installation

1. A recommended way to install ***@4geit/swg-check-availability-helper*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-check-availability-helper) package manager using the following command:

```bash
npm i @4geit/swg-check-availability-helper --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-check-availability-helper
```

2. All you have to do is to import the `@4geit/swg-check-availability-helper` package in your controller or mock file as below:

```js
// ...
const swgCheckAvailabilityHelper = require('@4geit/swg-check-availability-helper');
// ...
```

And use it with one of your endpoint controller as below:

```js
// ...
module.exports = {
  // ...
  getItems: swgCheckAvailabilityHelper(
    (req, res) => {
      // ...
    }
  ),
  // ...
};
```

# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.31.7"></a>
## [1.31.7](https://gitlab.com/4geit/swagger-packages/compare/v1.31.6...v1.31.7) (2017-12-01)


### Bug Fixes

* **contact-model, product-model:** wire pagination parameters ([7363430](https://gitlab.com/4geit/swagger-packages/commit/7363430))




<a name="1.31.5"></a>
## [1.31.5](https://gitlab.com/4geit/swagger-packages/compare/v1.31.4...v1.31.5) (2017-11-24)




**Note:** Version bump only for package @4geit/swg-message-model

<a name="1.31.3"></a>
## [1.31.3](https://gitlab.com/4geit/swagger-packages/compare/v1.31.2...v1.31.3) (2017-11-14)




**Note:** Version bump only for package @4geit/swg-message-model

<a name="1.31.1"></a>
## [1.31.1](https://gitlab.com/4geit/swagger-packages/compare/v1.31.0...v1.31.1) (2017-10-25)




**Note:** Version bump only for package @4geit/swg-message-model

<a name="1.31.0"></a>
# [1.31.0](https://gitlab.com/4geit/swagger-packages/compare/v1.30.2...v1.31.0) (2017-10-22)


### Features

* **jest:** convert to jest test environment ([95b423f](https://gitlab.com/4geit/swagger-packages/commit/95b423f))




<a name="1.28.1"></a>
## [1.28.1](https://gitlab.com/4geit/swagger-packages/compare/v1.28.0...v1.28.1) (2017-10-06)




**Note:** Version bump only for package @4geit/swg-message-model

<a name="1.28.0"></a>
# [1.28.0](https://gitlab.com/4geit/swagger-packages/compare/v1.27.0...v1.28.0) (2017-10-06)




**Note:** Version bump only for package @4geit/swg-message-model

<a name="1.27.0"></a>
# [1.27.0](https://gitlab.com/4geit/swagger-packages/compare/v1.26.0...v1.27.0) (2017-10-06)


### Features

* **message-model:** add isSender boolean ([fe9e5a8](https://gitlab.com/4geit/swagger-packages/commit/fe9e5a8))




<a name="1.23.0"></a>
# [1.23.0](https://gitlab.com/4geit/swagger-packages/compare/v1.22.1...v1.23.0) (2017-10-04)


### Features

* **message-model:** if one user-chatbox maximized add message only to its chatbox item ([abab11d](https://gitlab.com/4geit/swagger-packages/commit/abab11d))




<a name="1.22.0"></a>
# [1.22.0](https://gitlab.com/4geit/swagger-packages/compare/v1.21.0...v1.22.0) (2017-10-03)


### Features

* **message-model:** add messageBulkAdd endpoint ([eb92d06](https://gitlab.com/4geit/swagger-packages/commit/eb92d06))




<a name="1.21.0"></a>
# [1.21.0](https://gitlab.com/4geit/swagger-packages/compare/v1.20.0...v1.21.0) (2017-10-03)


### Features

* **message-model:** add CRUD operators ([aef559a](https://gitlab.com/4geit/swagger-packages/commit/aef559a))




<a name="1.15.1"></a>
## [1.15.1](https://gitlab.com/4geit/swagger-packages/compare/v1.15.0...v1.15.1) (2017-09-23)




**Note:** Version bump only for package @4geit/swg-message-model

<a name="1.15.0"></a>
# [1.15.0](https://gitlab.com/4geit/swagger-packages/compare/v1.14.0...v1.15.0) (2017-09-22)


### Features

* **message-model:** add message model + endpoints ([d55eabd](https://gitlab.com/4geit/swagger-packages/commit/d55eabd))

'use strict'

const swgCheckAvailabilityHelper = require('@4geit/swg-check-availability-helper')
const swgGetUserWithApiTokenHelper = require('@4geit/swg-get-user-with-api-token-helper')
const swgUseDbHelper = require('@4geit/swg-use-db-helper')
const swgDestructuringParametersHelper = require('@4geit/swg-destructuring-parameters-helper')

module.exports = {
  account: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          ({ req, res, user }) => {
            return res.json(user)
          }
        )
      )
    )
  )
}

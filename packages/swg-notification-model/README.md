# @4geit/swg-notification-model [![npm version](//badge.fury.io/js/@4geit%2Fswg-notification-model.svg)](//badge.fury.io/js/@4geit%2Fswg-notification-model)

---

notification model provides data model + endpoints for the notifications menu feature

## Installation

1. A recommended way to install ***@4geit/swg-notification-model*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-notification-model) package manager using the following command:

```bash
npm i @4geit/swg-notification-model --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-notification-model
```

2. In your swagger file, you need to add a reference to the `SwgNotificationModel` definition under the `definitions` property (e.g. `/api/swagger/swagger.yaml`) as below:

```yaml
swagger: "2.0"
# ...
definitions:
  SwgNotificationModel:
    $ref: ../../node_modules/@4geit/swg-notification-model/swagger.yaml#/definitions/SwgNotificationModel
# ...
```

3. SwgNotificationModel also comes along with some endpoints you can expose to the API, to do so you will need to add a reference to the `SwgNotificationModel` definition under the `paths` property (e.g. `/api/swagger/swagger.yaml`) as below:

```yaml
swagger: "2.0"
# ...
paths:
  /notification:
    $ref: ../../node_modules/@4geit/swg-notification-model/swagger.yaml#/definitions/NotificationListPath
  /notification/{id}:
    $ref: ../../node_modules/@4geit/swg-notification-model/swagger.yaml#/definitions/NotificationItemPath
  /notification/populate:
    $ref: ../../node_modules/@4geit/swg-notification-model/swagger.yaml#/definitions/PopulatePath
# ...
```

And you will also need to add the path to the `controllers` folder of the `swg-notification-model` package so that swagger-node will find the relevant controller to use. Edit the file `/config/default.yaml` and add two new paths to the properties `mockControllersDirs` and `controllersDirs` as illustrated below:

```yaml
swagger:
  # ...
  bagpipes:
    _router:
      # ...
      mockControllersDirs:
        # ...
        - node_modules/@4geit/swg-notification-model/mocks
        # ...
      controllersDirs:
        # ...
        - node_modules/@4geit/swg-notification-model/controllers
        # ...
```

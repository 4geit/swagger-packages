# @4geit/swg-reporter-helper [![npm version](//badge.fury.io/js/@4geit%2Fswg-reporter-helper.svg)](//badge.fury.io/js/@4geit%2Fswg-reporter-helper)

---

mocha custom spec reporter

## Installation

1. A recommended way to install ***@4geit/swg-reporter-helper*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-reporter-helper) package manager using the following command:

```bash
npm i @4geit/swg-reporter-helper --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-reporter-helper
```

2. All you have to do is to import the `@4geit/swg-reporter-helper` package in your controller or mock file as below:

```js
// ...
const swgReporterHelper = require('@4geit/swg-reporter-helper')
// ...
```

And use it with one of your endpoint controller as below:

```js
// ...
module.exports = {
  // ...
  getItems: swgDestructuringParametersHelper(
    swgReporterHelper(
      ({ req, res }) => {
        // ...
      }
    )
  ),
  // ...
}
```

# @4geit/swg-get-user-with-api-token-helper [![npm version](//badge.fury.io/js/@4geit%2Fswg-get-user-with-api-token-helper.svg)](//badge.fury.io/js/@4geit%2Fswg-get-user-with-api-token-helper)

---

return an account that matches the passed api token

## Installation

1. A recommended way to install ***@4geit/swg-get-user-with-api-token-helper*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-get-user-with-api-token-helper) package manager using the following command:

```bash
npm i @4geit/swg-get-user-with-api-token-helper --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-get-user-with-api-token-helper
```

2. All you have to do is to import the `@4geit/swg-get-user-with-api-token-helper` package in your controller or mock file as below:

```js
// ...
const swgGetUserWithApiTokenHelper = require('@4geit/swg-get-user-with-api-token-helper');
// ...
```

And use it with one of your endpoint controller as below:

```js
// ...
module.exports = {
  // ...
  getItems: swgGetUserWithApiTokenHelper(
    ({ req, res, user }) => {
      // ...
    }
  ),
  // ...
};
```

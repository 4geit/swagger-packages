'use strict'

const JsonRefs = require('json-refs')
const YAML = require('js-yaml')
const path = require('path')

module.exports = url => {
  // clear json-refs cache first
  JsonRefs.clearCache()
  // load swagger
  return JsonRefs.resolveRefsAt(url || path.resolve('./api/swagger/swagger.yaml'), {
    // Resolve all remote references
    filter: ['local', 'relative', 'remote'],
    loaderOptions: {
      processContent: (res, cb) => cb(undefined, YAML.safeLoad(res.text))
    }
  })
}

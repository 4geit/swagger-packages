# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.31.2"></a>
## [1.31.2](https://gitlab.com/4geit/swagger-packages/compare/v1.31.1...v1.31.2) (2017-11-14)




**Note:** Version bump only for package @4geit/swg-notification-model

<a name="1.31.1"></a>
## [1.31.1](https://gitlab.com/4geit/swagger-packages/compare/v1.31.0...v1.31.1) (2017-10-25)




**Note:** Version bump only for package @4geit/swg-notification-model

<a name="1.31.0"></a>
# [1.31.0](https://gitlab.com/4geit/swagger-packages/compare/v1.30.2...v1.31.0) (2017-10-22)


### Features

* **jest:** convert to jest test environment ([95b423f](https://gitlab.com/4geit/swagger-packages/commit/95b423f))




<a name="1.29.1"></a>
## [1.29.1](https://gitlab.com/4geit/swagger-packages/compare/v1.29.0...v1.29.1) (2017-10-09)


### Bug Fixes

* **swg-notification-model:** add enum for type and subtype ([5a0d3aa](https://gitlab.com/4geit/swagger-packages/commit/5a0d3aa))




<a name="1.28.2"></a>
## [1.28.2](https://gitlab.com/4geit/swagger-packages/compare/v1.28.1...v1.28.2) (2017-10-09)


### Bug Fixes

* **swg-notification-model:** changes to x-faker properties ([efc05d7](https://gitlab.com/4geit/swagger-packages/commit/efc05d7))




<a name="1.28.0"></a>
# [1.28.0](https://gitlab.com/4geit/swagger-packages/compare/v1.27.0...v1.28.0) (2017-10-06)


### Features

* **paginate:** add paginate to the list endpoints ([209a863](https://gitlab.com/4geit/swagger-packages/commit/209a863))




<a name="1.26.0"></a>
# [1.26.0](https://gitlab.com/4geit/swagger-packages/compare/v1.25.3...v1.26.0) (2017-10-05)


### Features

* **notification-model:** add notification model data model + endpoints ([b6d7d3f](https://gitlab.com/4geit/swagger-packages/commit/b6d7d3f))

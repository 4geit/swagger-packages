# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.31.2"></a>
## [1.31.2](https://gitlab.com/4geit/swagger-packages/compare/v1.31.1...v1.31.2) (2017-11-14)




**Note:** Version bump only for package @4geit/swg-todo-model

<a name="1.31.1"></a>
## [1.31.1](https://gitlab.com/4geit/swagger-packages/compare/v1.31.0...v1.31.1) (2017-10-25)




**Note:** Version bump only for package @4geit/swg-todo-model

<a name="1.31.0"></a>
# [1.31.0](https://gitlab.com/4geit/swagger-packages/compare/v1.30.2...v1.31.0) (2017-10-22)


### Features

* **jest:** convert to jest test environment ([95b423f](https://gitlab.com/4geit/swagger-packages/commit/95b423f))
* **script:** add test env to model package + todo model ([7af1b34](https://gitlab.com/4geit/swagger-packages/commit/7af1b34))




<a name="1.30.2"></a>
## [1.30.2](https://gitlab.com/4geit/swagger-packages/compare/v1.30.1...v1.30.2) (2017-10-12)


### Bug Fixes

* **todo:** fix issues ([aabad47](https://gitlab.com/4geit/swagger-packages/commit/aabad47))




<a name="1.30.1"></a>
## [1.30.1](https://gitlab.com/4geit/swagger-packages/compare/v1.30.0...v1.30.1) (2017-10-12)




**Note:** Version bump only for package @4geit/swg-todo-model

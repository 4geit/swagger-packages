'use strict'

module.exports = cb => {
  return async ({ req, res, db, ...fields }) => {
    const token = req.headers.token
    if (token === undefined) {
      // no token passed
      return res.status(401).json({
        message: 'No token.'
      })
    }

    // define db collection to use
    const users = db.collection('users')

    const user = await users.findOne({
      token: token
    })
    if (!user) {
      // send an error object if user is nil
      return res.status(401).json({
        message: 'Invalid token.'
      })
    }
    // if found a matching user, send it
    return cb({ req, res, db, user, ...fields })
  }
}

# @4geit/swg-use-db-helper [![npm version](//badge.fury.io/js/@4geit%2Fswg-use-db-helper.svg)](//badge.fury.io/js/@4geit%2Fswg-use-db-helper)

---

pass the db pointer to an endpoint controller

## Installation

1. A recommended way to install ***@4geit/swg-use-db-helper*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-use-db-helper) package manager using the following command:

```bash
npm i @4geit/swg-use-db-helper --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-use-db-helper
```

2. All you have to do is to import the `@4geit/swg-use-db-helper` package in your controller or mock file as below:

```js
// ...
const swgUseDbHelper = require('@4geit/swg-use-db-helper');
// ...
```

And use it with one of your endpoint controller as below:

```js
// ...
module.exports = {
  // ...
  getItems: swgUseDbHelper(
    (req, res) => {
      // ...
    }
  ),
  // ...
};
```

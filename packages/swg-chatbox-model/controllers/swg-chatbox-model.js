'use strict'

const jsf = require('json-schema-faker')
const faker = require('faker')
faker.seed(24)
const Chance = require('chance')
const chance = new Chance(24)
const path = require('path')

const swgCheckAvailabilityHelper = require('@4geit/swg-check-availability-helper')
const swgUseDbHelper = require('@4geit/swg-use-db-helper')
const swgDereferenceSwaggerHelper = require('@4geit/swg-dereference-swagger-helper')
const swgGetUserWithApiTokenHelper = require('@4geit/swg-get-user-with-api-token-helper');
const swgDestructuringParametersHelper = require('@4geit/swg-destructuring-parameters-helper')

module.exports = {
  chatboxList: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              // define db collection to use
              const collection = db.collection('chatbox')
              // get all items
              const docs = await collection.find({
                user_id: user._id,
              }).toArray()
              // embeed
              const resolution = await Promise.all(docs.map(async ({ /*other_id,*/ user_id, ...fields }) => ({
                ...fields,
                user,
                // other: await others.findOne({ _id: other_id }),
              })))
              // return result
              res.json(resolution)
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  chatboxAdd: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const { _id, id, user_id, /*otherId,*/ ...fields } = req.swagger.params.body.value
              // define db collection to use
              // const otherCollection = db.collection('others')
              const collection = db.collection('chatbox')
              // find other
              // const otherItem = await otherCollection.findOne({
              //   id: otherId,
              // })
              // if (!otherItem) {
              //   // send an error object if otherItem is nil
              //   return res.status(403).json({
              //     message: 'Invalid other ID.'
              //   })
              // }
              // add the new user chatbox to the collection
              await collection.insert({
                id: chance.guid(),
                user_id: user._id,
                // other_id: other._id,
                ...fields,
              })
              // return message
              return res.status(201).json({
                message: 'added chatbox item!'
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  chatboxGet: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const id = req.swagger.params.id.value
              // define db collection to use
              // const otherCollection = db.collection('others')
              const collection = db.collection('chatbox')
              // find item
              const item = await collection.findOne({
                id,
                user_id: user._id
              })
              if (!item) {
                // send an error object if item is nil
                return res.status(403).json({
                  message: 'Invalid chatbox ID.'
                })
              }
              // embeed
              const { /*other_id,*/ user_id, ...fields } = item
              const resolution = {
                ...fields,
                user,
                // other: await otherCollection.findOne({ _id: other_id }),
              }
              // return item
              return res.status(200).json(resolution)
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  chatboxDelete: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const id = req.swagger.params.id.value
              // define db collection to use
              const collection = db.collection('chatbox')
              // delete item
              await collection.deleteOne({
                id,
                user_id: user._id
              })
              // return message
              return res.status(200).json({
                message: 'chatbox deleted!'
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  chatboxUpdate: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              const itemId = req.swagger.params.id.value
              const { _id, id, user_id, /*other_id,*/ ...fields } = req.swagger.params.body.value
              // define db collection to use
              const collection = db.collection('chatbox')
              // udptae item
              if (!Object.keys(fields).length) {
                // send an error object if update failed
                return res.status(500).json({
                  message: 'No field to update.'
                })
              }
              const result = await collection.updateOne({
                id: itemId,
                user_id: user._id,
              }, { $set: { ...fields } })
              if (!result.modifiedCount) {
                // send an error object if update failed
                return res.status(500).json({
                  message: 'Update failed.'
                })
              }
              // return item
              return res.status(200).json({
                message: 'Update succeeded.'
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  ),
  chatboxPopulate: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        swgGetUserWithApiTokenHelper(
          async ({ req, res, db, user }) => {
            try {
              // define db collection to use
              const collection = db.collection('chatbox')
              // resolve swagger
              const { resolved: { definitions: { SwgChatboxModel } } } = await swgDereferenceSwaggerHelper(path.resolve(__dirname, '../swagger.yaml'))
              // add custom faker generator
              jsf.extend('faker', () => {
                faker.custom = {}
                return faker
              })
              // add custom chance generator
              jsf.extend('chance', () => {
                const custom = {
                  item: () => jsf(SwgChatboxModel),
                  setTrue: () => true,
                  setFalse: () => false,
                  tz: () => chance.timezone().abbr,
                }
                chance.mixin(custom)
                return chance
              })
              // create fake items and insert them to db
              await collection.insertMany(
                chance.n(chance.item, 10).map(({ user: dummyUser, ...fields }) => ({ user_id: user._id, ...fields }))
              )
              // return success message
              res.status(201).json({
                message: "Succeeded to populate fake chatbox data to the db."
              })
            } catch (err) {
              console.log(err)
              res.status(500).json({
                message: err
              })
            }
          }
        )
      )
    )
  )
}

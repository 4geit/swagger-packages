# @4geit/swg-allow-cross-domain-helper [![npm version](//badge.fury.io/js/@4geit%2Fswg-allow-cross-domain-helper.svg)](//badge.fury.io/js/@4geit%2Fswg-allow-cross-domain-helper)

---

set CORS headers

## Installation

1. A recommended way to install ***@4geit/swg-allow-cross-domain-helper*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-allow-cross-domain-helper) package manager using the following command:

```bash
npm i @4geit/swg-allow-cross-domain-helper --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-allow-cross-domain-helper
```

2. All you have to do is to import the `@4geit/swg-allow-cross-domain-helper` package in your controller or mock file as below:

```js
// ...
const swgAllowCrossDomainHelper = require('@4geit/swg-allow-cross-domain-helper');
// ...
```

And use it with one of your endpoint controller as below:

```js
// ...
module.exports = {
  // ...
  getItems: swgAllowCrossDomainHelper(
    (req, res) => {
      // ...
    }
  ),
  // ...
};
```

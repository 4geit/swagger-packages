# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.31.2"></a>
## [1.31.2](https://gitlab.com/4geit/swagger-packages/compare/v1.31.1...v1.31.2) (2017-11-14)




**Note:** Version bump only for package @4geit/swg-account-model

<a name="1.31.1"></a>
## [1.31.1](https://gitlab.com/4geit/swagger-packages/compare/v1.31.0...v1.31.1) (2017-10-25)




**Note:** Version bump only for package @4geit/swg-account-model

<a name="1.31.0"></a>
# [1.31.0](https://gitlab.com/4geit/swagger-packages/compare/v1.30.2...v1.31.0) (2017-10-22)


### Features

* **jest:** convert to jest test environment ([95b423f](https://gitlab.com/4geit/swagger-packages/commit/95b423f))




<a name="1.28.0"></a>
# [1.28.0](https://gitlab.com/4geit/swagger-packages/compare/v1.27.0...v1.28.0) (2017-10-06)




**Note:** Version bump only for package @4geit/swg-account-model

<a name="1.15.1"></a>
## [1.15.1](https://gitlab.com/4geit/swagger-packages/compare/v1.15.0...v1.15.1) (2017-09-23)




**Note:** Version bump only for package @4geit/swg-account-model

<a name="1.10.1"></a>
## [1.10.1](https://gitlab.com/4geit/swagger-packages/compare/v1.10.0...v1.10.1) (2017-09-18)




**Note:** Version bump only for package @4geit/swg-account-model

<a name="1.7.0"></a>
# [1.7.0](https://gitlab.com/4geit/swagger-packages/compare/v1.6.3...v1.7.0) (2017-08-27)


### Features

* **register:** add register path logic ([be7ca7d](https://gitlab.com/4geit/swagger-packages/commit/be7ca7d))




<a name="1.6.3"></a>
## [1.6.3](https://gitlab.com/4geit/swagger-packages/compare/v1.6.2...v1.6.3) (2017-08-27)


### Bug Fixes

* **async/await:** support async/await ([61d7998](https://gitlab.com/4geit/swagger-packages/commit/61d7998))




<a name="1.5.0"></a>
# [1.5.0](https://gitlab.com/4geit/swagger-packages/compare/v1.4.0...v1.5.0) (2017-08-27)




**Note:** Version bump only for package @4geit/swg-account-model

<a name="1.4.0"></a>
# 1.4.0 (2017-08-27)


### Features

* **account-model:** add account model ([b746526](https://gitlab.com/4geit/swagger-packages/commit/b746526))

# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.31.7"></a>
## [1.31.7](https://gitlab.com/4geit/swagger-packages/compare/v1.31.6...v1.31.7) (2017-12-01)




**Note:** Version bump only for package @4geit/swg-user-chatbox-model

<a name="1.31.6"></a>
## [1.31.6](https://gitlab.com/4geit/swagger-packages/compare/v1.31.5...v1.31.6) (2017-11-24)


### Bug Fixes

* **user-chatbox,eslint:** add user-chatbox bulk endpoints, add eslint support ([16acdd4](https://gitlab.com/4geit/swagger-packages/commit/16acdd4))




<a name="1.31.5"></a>
## [1.31.5](https://gitlab.com/4geit/swagger-packages/compare/v1.31.4...v1.31.5) (2017-11-24)




**Note:** Version bump only for package @4geit/swg-user-chatbox-model

<a name="1.31.3"></a>
## [1.31.3](https://gitlab.com/4geit/swagger-packages/compare/v1.31.2...v1.31.3) (2017-11-14)




**Note:** Version bump only for package @4geit/swg-user-chatbox-model

<a name="1.31.2"></a>
## [1.31.2](https://gitlab.com/4geit/swagger-packages/compare/v1.31.1...v1.31.2) (2017-11-14)




**Note:** Version bump only for package @4geit/swg-user-chatbox-model

<a name="1.31.1"></a>
## [1.31.1](https://gitlab.com/4geit/swagger-packages/compare/v1.31.0...v1.31.1) (2017-10-25)




**Note:** Version bump only for package @4geit/swg-user-chatbox-model

<a name="1.31.0"></a>
# [1.31.0](https://gitlab.com/4geit/swagger-packages/compare/v1.30.2...v1.31.0) (2017-10-22)


### Features

* **jest:** convert to jest test environment ([95b423f](https://gitlab.com/4geit/swagger-packages/commit/95b423f))




<a name="1.28.1"></a>
## [1.28.1](https://gitlab.com/4geit/swagger-packages/compare/v1.28.0...v1.28.1) (2017-10-06)




**Note:** Version bump only for package @4geit/swg-user-chatbox-model

<a name="1.28.0"></a>
# [1.28.0](https://gitlab.com/4geit/swagger-packages/compare/v1.27.0...v1.28.0) (2017-10-06)




**Note:** Version bump only for package @4geit/swg-user-chatbox-model

<a name="1.27.0"></a>
# [1.27.0](https://gitlab.com/4geit/swagger-packages/compare/v1.26.0...v1.27.0) (2017-10-06)




**Note:** Version bump only for package @4geit/swg-user-chatbox-model

<a name="1.25.0"></a>
# [1.25.0](https://gitlab.com/4geit/swagger-packages/compare/v1.24.0...v1.25.0) (2017-10-05)


### Features

* **user-chatbox-model:** add messages field ([535957c](https://gitlab.com/4geit/swagger-packages/commit/535957c))




<a name="1.24.0"></a>
# [1.24.0](https://gitlab.com/4geit/swagger-packages/compare/v1.23.0...v1.24.0) (2017-10-04)


### Features

* **user-chatbox-model:** add maximized filter param ([241b0a4](https://gitlab.com/4geit/swagger-packages/commit/241b0a4))




<a name="1.22.1"></a>
## [1.22.1](https://gitlab.com/4geit/swagger-packages/compare/v1.22.0...v1.22.1) (2017-10-03)


### Bug Fixes

* **user-chatbox:** disable populate endpoint ([8bdbdb0](https://gitlab.com/4geit/swagger-packages/commit/8bdbdb0))




<a name="1.20.0"></a>
# [1.20.0](https://gitlab.com/4geit/swagger-packages/compare/v1.19.5...v1.20.0) (2017-09-27)


### Bug Fixes

* **user-chatbox-model:** rename order field to position ([b595da3](https://gitlab.com/4geit/swagger-packages/commit/b595da3))


### Features

* **chatbox-model:** add new status endpoint to know if a chatbox has been already added to a user l ([362d0d9](https://gitlab.com/4geit/swagger-packages/commit/362d0d9))




<a name="1.19.5"></a>
## [1.19.5](https://gitlab.com/4geit/swagger-packages/compare/v1.19.4...v1.19.5) (2017-09-26)


### Bug Fixes

* **user-chatbox-model:** minor fix ([77181bd](https://gitlab.com/4geit/swagger-packages/commit/77181bd))




<a name="1.19.4"></a>
## [1.19.4](https://gitlab.com/4geit/swagger-packages/compare/v1.19.3...v1.19.4) (2017-09-26)


### Bug Fixes

* **swagger:** missing security def ([82627d6](https://gitlab.com/4geit/swagger-packages/commit/82627d6))




<a name="1.19.0"></a>
# [1.19.0](https://gitlab.com/4geit/swagger-packages/compare/v1.18.0...v1.19.0) (2017-09-24)


### Features

* **product/contact:** CRUD operators/endpoints ([5b5d978](https://gitlab.com/4geit/swagger-packages/commit/5b5d978))




<a name="1.18.0"></a>
# [1.18.0](https://gitlab.com/4geit/swagger-packages/compare/v1.17.0...v1.18.0) (2017-09-24)


### Features

* **user-chatbox-model:** add minor field ([3d9f1eb](https://gitlab.com/4geit/swagger-packages/commit/3d9f1eb))




<a name="1.17.0"></a>
# [1.17.0](https://gitlab.com/4geit/swagger-packages/compare/v1.16.0...v1.17.0) (2017-09-24)


### Features

* **user-chatbox:** add update endpoint + add update endpoint to model generation script ([f66f5a9](https://gitlab.com/4geit/swagger-packages/commit/f66f5a9))




<a name="1.16.0"></a>
# [1.16.0](https://gitlab.com/4geit/swagger-packages/compare/v1.15.2...v1.16.0) (2017-09-24)


### Features

* **scripts:** add CRUD controllers to scripts ([d143fde](https://gitlab.com/4geit/swagger-packages/commit/d143fde))
* **user-chatbox:** add get and delete endpoint ([84d8723](https://gitlab.com/4geit/swagger-packages/commit/84d8723))




<a name="1.15.2"></a>
## [1.15.2](https://gitlab.com/4geit/swagger-packages/compare/v1.15.1...v1.15.2) (2017-09-23)


### Bug Fixes

* **user-chatbox-model:** add endpoint finished ([2310434](https://gitlab.com/4geit/swagger-packages/commit/2310434))




<a name="1.15.1"></a>
## [1.15.1](https://gitlab.com/4geit/swagger-packages/compare/v1.15.0...v1.15.1) (2017-09-23)




**Note:** Version bump only for package @4geit/swg-user-chatbox-model

<a name="1.10.1"></a>
## [1.10.1](https://gitlab.com/4geit/swagger-packages/compare/v1.10.0...v1.10.1) (2017-09-18)




**Note:** Version bump only for package @4geit/swg-user-chatbox-model

<a name="1.10.0"></a>
# [1.10.0](https://gitlab.com/4geit/swagger-packages/compare/v1.9.0...v1.10.0) (2017-09-18)


### Features

* **user-chatbox-model:** add listing + populate endpoints ([1f307c1](https://gitlab.com/4geit/swagger-packages/commit/1f307c1))




<a name="1.9.0"></a>
# [1.9.0](https://gitlab.com/4geit/swagger-packages/compare/v1.8.0...v1.9.0) (2017-09-18)


### Features

* **swg-user-chatbox-model:** add new model ([d4c6f23](https://gitlab.com/4geit/swagger-packages/commit/d4c6f23))

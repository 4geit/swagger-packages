# @4geit/swg-get-user-with-credentials-helper [![npm version](//badge.fury.io/js/@4geit%2Fswg-get-user-with-credentials-helper.svg)](//badge.fury.io/js/@4geit%2Fswg-get-user-with-credentials-helper)

---

return an account that corresponds to the passed credentials

## Installation

1. A recommended way to install ***@4geit/swg-get-user-with-credentials-helper*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-get-user-with-credentials-helper) package manager using the following command:

```bash
npm i @4geit/swg-get-user-with-credentials-helper --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-get-user-with-credentials-helper
```

2. All you have to do is to import the `@4geit/swg-get-user-with-credentials-helper` package in your controller or mock file as below:

```js
// ...
const swgGetUserWithCredentialsHelper = require('@4geit/swg-get-user-with-credentials-helper');
// ...
```

And use it with one of your endpoint controller as below:

```js
// ...
module.exports = {
  // ...
  getItems: swgGetUserWithCredentialsHelper(
    ({ req, res, user }) => {
      // ...
    }
  ),
  // ...
};
```

'use strict'

const jsf = require('json-schema-faker')
const faker = require('faker')
faker.seed(24)
const Chance = require('chance')
const chance = new Chance(24)
const path = require('path')

const swgCheckAvailabilityHelper = require('@4geit/swg-check-availability-helper')
const swgUseDbHelper = require('@4geit/swg-use-db-helper')
const swgDereferenceSwaggerHelper = require('@4geit/swg-dereference-swagger-helper')
const swgDestructuringParametersHelper = require('@4geit/swg-destructuring-parameters-helper')

module.exports = {
  register: swgDestructuringParametersHelper(
    swgCheckAvailabilityHelper(
      swgUseDbHelper(
        async ({ req, res, db }) => {
          try {
            const account = req.swagger.params.account.value
            const { email, password, firstname, lastname } = account
            // define db collection to use
            const users = db.collection('users')
            // test if email already exists
            const user = await users.findOne({ email })
            if (user) {
              // send an error object if user exists
              return res.status(400).json({
                message: 'Email already exists!'
              })
            }
            // resolve swagger
            const { resolved: { definitions: { SwgAccountModel } } } = await swgDereferenceSwaggerHelper('./node_modules/@4geit/swg-account-model/swagger.yaml')
            // add custom faker generator
            jsf.extend('faker', () => {
              faker.custom = {}
              return faker
            })
            // add custom chance generator
            jsf.extend('chance', () => {
              const custom = {
                user: () => jsf(SwgAccountModel),
                setTrue: () => true,
                setFalse: () => false,
              }
              chance.mixin(custom)
              return chance
            })
            const newUser = chance.user()
            Object.assign(newUser, {
              email,
              password,
              firstname,
              lastname
            })
            // create new account
            await users.insert(newUser)
            // return message
            return res.status(201).json({
              message: 'Account created!'
            })
          } catch (err) {
            console.log(err)
            res.status(500).json({
              message: err
            })
          }
        }
      )
    )
  )
}

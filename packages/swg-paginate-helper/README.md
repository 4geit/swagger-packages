# @4geit/swg-paginate-helper [![npm version](//badge.fury.io/js/@4geit%2Fswg-paginate-helper.svg)](//badge.fury.io/js/@4geit%2Fswg-paginate-helper)

---

wrapper function to enable pagination for an endpoint that returns a list of items

## Installation

1. A recommended way to install ***@4geit/swg-paginate-helper*** is through [npm](//www.npmjs.com/search?q=@4geit/swg-paginate-helper) package manager using the following command:

```bash
npm i @4geit/swg-paginate-helper --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/swg-paginate-helper
```

2. All you have to do is to import the `@4geit/swg-paginate-helper` package in your controller or mock file as below:

```js
// ...
const swgPaginateHelper = require('@4geit/swg-paginate-helper')
// ...
```

And use it with one of your endpoint controller as below:

```js
// ...
module.exports = {
  // ...
  getItems: swgPaginateHelper(
    ({ req, res, paginatedArr }) => {
      // ...
    }
  ),
  // ...
}
```
